################################################################################
# Build configuration for CxAODReader_Zemu
################################################################################

# Declare the name of the package:
atlas_subdir( CxAODReader_Zemu )

# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC 
   CxAODReader
   CxAODTools_Zemu
   KinematicFit
   NNLOReweighter
   Reconstruction/Jet/JetUncertainties
#   Reconstruction/Jet/JetCPInterfaces
   )

find_package( ROOT COMPONENTS Core Tree MathCore Hist Physics TMVA XMLParser XMLIO)

# Build a dictionary for the library:
atlas_add_root_dictionary( CxAODReader_Zemu _dictionarySource
   ROOT_HEADERS Root/LinkDef.h
   EXTERNAL_PACKAGES ROOT )

# Build a shared library:
atlas_add_library( CxAODReader_Zemu
   CxAODReader_Zemu/*.h Root/*.h Root/*.cxx ${_dictionarySource}
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   PUBLIC_HEADERS CxAODReader_Zemu
#   LINK_LIBRARIES ${ROOT_LIBRARIES} CxAODReader CxAODTools_Zemu KinematicFit NNLOReweighter JetUncertaintiesLib JetCPInterfacesLib)
LINK_LIBRARIES ${ROOT_LIBRARIES} CxAODReader CxAODTools_Zemu KinematicFit NNLOReweighterLib JetUncertaintiesLib)

atlas_install_generic( data/*
   DESTINATION data PKGNAME_SUBDIR )
