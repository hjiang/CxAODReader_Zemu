#include <EventLoop/Worker.h>
#include <typeinfo>
#include "TSystem.h"

#include <CxAODReader_Zemu/AnalysisReader_Zemu.h>

#include "CxAODTools_Zemu/EventSelection_Zemu.h"

//#include "CxAODTools/TriggerTool.h"
#include "CxAODTools_Zemu/TriggerTool_Zemu.h"

//#include "CxAODTools_LLP/LLPProperties.h"
#include "CxAODTools/CommonProperties.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODTruth/TruthVertex.h"

#include "CxxUtils/fpcompare.h"


#define length(array) (sizeof(array)/sizeof(*(array)))
double GeV = 1000.0;

ClassImp(AnalysisReader_Zemu)

AnalysisReader_Zemu::AnalysisReader_Zemu() :
  //AnalysisReader()//,
  m_analysisType(""),
  m_use2DbTagCut(false),
  m_hist_normalisation(nullptr)
  //m_pileupreweighting(nullptr),
  //m_muon_trig_sf(nullptr)
{
}




EL::StatusCode AnalysisReader_Zemu :: fileExecute (){
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  //std::cout << "fileExecute Input file is " << wk()->inputFile()->GetName() << std::endl;
  //
  TFile* inputfile=wk()->inputFile();
  TH1D* h = (TH1D*) inputfile->Get("MetaData_EventCount");
  if(h){
    if (!m_hist_normalisation) {
      m_hist_normalisation = (TH1D*) h->Clone();
      if (m_debug) Info("fileExecute()", "Made the cutflow hist");
    } else {
      m_hist_normalisation->Add(h);
    }
  }
  delete h;

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode AnalysisReader_Zemu :: initializeSelection(){
  m_config->getif<string>("analysisType", m_analysisType);

  if (m_debug) Info("initializeSelection()", "Initialize analysis '%s'.", m_analysisType.c_str());

    if( m_analysisType == "Zemu" ){
        m_eventSelection = new EventSelection_Zemu();
        m_fillFunction = std::bind(&AnalysisReader_Zemu::fill_Zemu, this);
    }else{
        Error("initializeSelection()", "Invalid analysis type %s", m_analysisType.c_str());
        return EL::StatusCode::FAILURE;
    }//end if-else

    // TODO move to base class?
    bool writeMVATree = false;
    bool readMVA = false;
    m_config->getif< bool >("writeMVATree", writeMVATree);
    m_config->getif< bool >("readMVA", readMVA);
	m_config->getif<bool>("use2DbTagCut", m_use2DbTagCut);

    //TTree
	//int int_analysisType;
	//if(m_analysisType == "Zemu")
	//	int_analysisType = 1 ;
    m_tree = new MVATree_Zemu(writeMVATree, readMVA, m_analysisType, wk(), m_variations, false);

    return EL::StatusCode::SUCCESS;
}


EL::StatusCode AnalysisReader_Zemu::initializeTools ()
{
  EL_CHECK("AnalysisReader_Zemu::initializeTools()", AnalysisReader::initializeTools());

  m_triggerTool = new TriggerTool_Zemu(*m_config);
  EL_CHECK("AnalysisReader_Zemu::initializeTools()", m_triggerTool -> initialize());

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisReader_Zemu :: histInitialize(){
    // histogram manager
  m_histSvc = new HistSvc();
  m_histNameSvc = new HistNameSvc();
  m_histSvc -> SetNameSvc(m_histNameSvc);
  m_histSvc -> SetWeightSysts(&m_weightSysts);

  static std::string cuts [24] = {"All","Event Trigger","Has Lepton","CRJet","TruthInfo","Trigger","TriggerMatch","OneFatJet","MET>100GeV","pTW>200GeV","pTW>0.4MlvJ","pTJ>0.4MlvJ","isWZJet","NBjet==0","BCtrl1","BCtrl2","BCtrl3","BCtrl4","WCtrl1","WCtrl2", "WCtrl3", "WCtrl4","SR3", "SR4"};
  static int n_cuts = 24;

  static std::string cuts_2lep[12] = {"All","passTrigger","PrimaryVertex", "Has Lepton", "veto additional lepton", "1 el 1 mu", "least 1 lep match trigger", "opposite charge", "bjet-veto",  "met<20GeV", "pTmax Jet<30GeV"};
  static int n_cuts_2lep = 12;

  static std::string cuts_el[13] = {"All", "passTrigger", "PrimaryVertex", "isGoodOQ", "pt>27", "|eta|<2.47 and 1.37,1.52", "|z0sinTheta|<0.5", "|d0_sig|<5", "passOR", "isLooseLH", "isMediumLH", "isGradientIso", ">1 el"};
  static int n_cuts_el = 13;

  static std::string cuts_mu[13] = {"All", "passTrigger", "PrimaryVertex", "pt>27", "Combined Muon", "muon Quality", "|eta|<2.5", "|z0sinTheta|<0.5", "|d0_sig|<3", "passOR", "isGradientLooseIso", "isGradientIso", ">1 mu"};
  static int n_cuts_mu = 13;

  static std::string cuts_jet[7] = {"All","passTrigger","PrimaryVertex", "OverlapRemoval e/mu", "|eta|<2.5", "Jvt>0.59"};
  static int n_cuts_jet = 7;

  m_hist_1lep_CutFlow[0] = new TH1D("CutFlow_e","CutFlow_e", n_cuts, 0.5, n_cuts+0.5);
  m_hist_1lep_CutFlow[0]->Sumw2();
  m_hist_1lep_CutFlow[1] = new TH1D("CutFlow_m","CutFlow_m", n_cuts, 0.5, n_cuts+0.5);
  m_hist_1lep_CutFlow[1]->Sumw2();
  m_hist_1lep_CutFlow[2] = new TH1D("CutFlow_combined","CutFlow_combined", n_cuts, 0.5, n_cuts+0.5);
  m_hist_1lep_CutFlow[2]->Sumw2();
  for(int ich=0; ich<3; ich++){
    for(int i=0; i<n_cuts; i++) {
      m_hist_1lep_CutFlow[ich]->GetXaxis()->SetBinLabel(i+1,cuts[i].c_str());
    }
  }

  m_hist_1lep_CutFlow_noWght[0] = new TH1D("CutFlow_noWeight_e","CutFlow_noWeight_e", n_cuts, 0.5, n_cuts+0.5);
  m_hist_1lep_CutFlow_noWght[0]->Sumw2();
  m_hist_1lep_CutFlow_noWght[1] = new TH1D("CutFlow_noWeight_m","CutFlow_noWeight_m", n_cuts, 0.5, n_cuts+0.5);
  m_hist_1lep_CutFlow_noWght[1]->Sumw2();
  m_hist_1lep_CutFlow_noWght[2] = new TH1D("CutFlow_noWeight_combined","CutFlow_noWeight_combined", n_cuts, 0.5, n_cuts+0.5);
  m_hist_1lep_CutFlow_noWght[2]->Sumw2();
  for(int ich=0; ich<3; ich++){
    for(int i=0; i<n_cuts; i++) {
      m_hist_1lep_CutFlow_noWght[ich]->GetXaxis()->SetBinLabel(i+1,cuts[i].c_str());
    }
  }

  ////////////////////////// my own cutflow and histograms /////////////////////////
  // event cutflow 
  m_hist_2lep_CutFlow_noWght = new TH1D("CutFlow_2lep_noWeight","CutFlow_2lep_noWeight", n_cuts_2lep, 0.5, n_cuts_2lep+0.5);
  m_hist_2lep_CutFlow = new TH1D("CutFlow_2lep","CutFlow_2lep", n_cuts_2lep, 0.5, n_cuts_2lep+0.5);
  m_hist_2lep_CutFlow_noWght->Sumw2();
  m_hist_2lep_CutFlow->Sumw2();
  for(int i=0; i<n_cuts_2lep; i++) {
    m_hist_2lep_CutFlow_noWght->GetXaxis()->SetBinLabel(i+1,cuts_2lep[i].c_str());
    m_hist_2lep_CutFlow->GetXaxis()->SetBinLabel(i+1,cuts_2lep[i].c_str());
  }
  // electron object cutflow
  m_hist_el_CutFlow_noWght = new TH1D("CutFlow_el_noWeight","CutFlow_el_noWeight", n_cuts_el, 0.5, n_cuts_el+0.5);
  m_hist_el_CutFlow_noWght->Sumw2();
  for(int i=0; i<n_cuts_el; i++) {
    m_hist_el_CutFlow_noWght->GetXaxis()->SetBinLabel(i+1,cuts_el[i].c_str());
  }
  m_hist_el_CutFlow_withWght = new TH1D("CutFlow_el_withWeight","CutFlow_el_withWeight", n_cuts_el, 0.5, n_cuts_el+0.5);
  m_hist_el_CutFlow_withWght->Sumw2();
  for(int i=0; i<n_cuts_el; i++) {
    m_hist_el_CutFlow_withWght->GetXaxis()->SetBinLabel(i+1,cuts_el[i].c_str());
  }

  // muon object cutflow
  m_hist_mu_CutFlow_noWght = new TH1D("CutFlow_mu_noWeight","CutFlow_mu_noWeight", n_cuts_mu, 0.5, n_cuts_mu+0.5);
  m_hist_mu_CutFlow_noWght->Sumw2();
  for(int i=0; i<n_cuts_mu; i++) {
    m_hist_mu_CutFlow_noWght->GetXaxis()->SetBinLabel(i+1,cuts_mu[i].c_str());
  }
  m_hist_mu_CutFlow_withWght = new TH1D("CutFlow_mu_withWeight","CutFlow_mu_withWeight", n_cuts_mu, 0.5, n_cuts_mu+0.5);
  m_hist_mu_CutFlow_withWght->Sumw2();
  for(int i=0; i<n_cuts_mu; i++) {
    m_hist_mu_CutFlow_withWght->GetXaxis()->SetBinLabel(i+1,cuts_mu[i].c_str());
  }

  // jet object cutflow
  m_hist_jet_CutFlow_noWght = new TH1D("CutFlow_jet_noWeight","CutFlow_jet_noWeight", n_cuts_jet, 0.5, n_cuts_jet+0.5);
  m_hist_jet_CutFlow_noWght->Sumw2();
  for(int i=0; i<n_cuts_jet; i++) {
    m_hist_jet_CutFlow_noWght->GetXaxis()->SetBinLabel(i+1,cuts_jet[i].c_str());
  }
  m_hist_jet_CutFlow_withWght = new TH1D("CutFlow_jet_withWeight","CutFlow_jet_withWeight", n_cuts_jet, 0.5, n_cuts_jet+0.5);
  m_hist_jet_CutFlow_withWght->Sumw2();
  for(int i=0; i<n_cuts_jet; i++) {
    m_hist_jet_CutFlow_withWght->GetXaxis()->SetBinLabel(i+1,cuts_jet[i].c_str());
  }


  ////////////////// below are all the event info and kinematic variables
  // the range should be long enough and the bins should be always large, need to save for the plotting later

  // the averageInteractionsPerCrossing check 
  m_hist_averageInteractionsPerCrossing = new TH1D("mu", "mu", 50, 0, 50 );
  m_hist_averageInteractionsPerCrossing->Sumw2();
  // the averageInteractionsPerCrossing check
  m_hist_averageInteractionsPerCrossingRecalc = new TH1D("mu_Recalc", "mu_Recalc", 50, 0, 50 );
  m_hist_averageInteractionsPerCrossingRecalc->Sumw2();
  // the Pileupweight
  m_hist_Pileupweight = new TH1D("Pileupweight", "Pileupweight", 50, 0, 50);
  m_hist_Pileupweight->Sumw2();
  // the PileupweightRecalc
  m_hist_PileupweightRecalc = new TH1D("PileupweightRecalc", "PileupweightRecalc", 50, 0, 50);
  m_hist_PileupweightRecalc->Sumw2();

  ///////////// below are the histograms for ploting(after all the cuts)
  // event info
  m_hist_EventWeight = new TH1D("EventWeight", "EventWeight", 400, -200, 200);
  m_hist_EventWeight->Sumw2();

  m_hist_MCEventWeight = new TH1D("MCEventWeight", "MCEventWeight", 200, -100, 100);
  m_hist_EventWeight->Sumw2();

  m_hist_LumiWeight = new TH1D("LumiWeight", "LumiWeight", 400, -200, 200);
  m_hist_LumiWeight->Sumw2();

  m_hist_PileupWeight = new TH1D("PileupWeight", "PileupWeight", 200, 0, 40);
  m_hist_PileupWeight->Sumw2();

  m_hist_averageInteractionsPerCrossingRecalc_afterCuts = new TH1D("mu_Recalc_afterCuts", "mu_Recalc_afterCuts", 50, 0, 50 );
  m_hist_averageInteractionsPerCrossingRecalc_afterCuts->Sumw2();

  m_hist_leptonSF_el = new TH1D("leptonSF_el", "leptonSF_el", 200, 0, 40);
  m_hist_leptonSF_el->Sumw2();

  m_hist_leptonSF_mu = new TH1D("leptonSF_mu", "leptonSF_mu", 200, 0, 40);
  m_hist_leptonSF_mu->Sumw2();

  // electron kinematics(weighted)
  m_hist_nElectrons = new TH1D("nElectrons", "nElectrons", 4, 0, 4); 
  m_hist_nElectrons->Sumw2();

  m_hist_electron_pT = new TH1D("electron_pT", "electron_pT", 200, 0, 200); 
  m_hist_electron_pT->Sumw2();

  m_hist_electron_e = new TH1D("electron_e", "electron_e", 300, 0, 600); 
  m_hist_electron_e->Sumw2();

  m_hist_electron_phi = new TH1D("electron_phi", "electron_phi", 80, -4, 4); 
  m_hist_electron_phi->Sumw2();

  m_hist_electron_eta = new TH1D("electron_eta", "electron_eta", 80, -4, 4); 
  m_hist_electron_eta->Sumw2();

  // muon kinematics(weighted)
  m_hist_nMuons = new TH1D("nMuons", "nMuons", 4, 0, 4); 
  m_hist_nMuons->Sumw2();

  m_hist_muon_pT = new TH1D("muon_pT", "muon_pT", 200, 0, 200); 
  m_hist_muon_pT->Sumw2();

  m_hist_muon_e = new TH1D("muon_e", "muon_e", 300, 0, 600); 
  m_hist_muon_e->Sumw2();

  m_hist_muon_phi = new TH1D("muon_phi", "muon_phi", 80, -4, 4); 
  m_hist_muon_phi->Sumw2();

  m_hist_muon_eta = new TH1D("muon_eta", "muon_eta", 80, -4, 4); 
  m_hist_muon_eta->Sumw2();

  // jet kinematics(weighted)
  m_hist_nJets = new TH1D("nJets", "nJets", 14, 0, 14); 
  m_hist_nJets->Sumw2();
  //m_hist_nbjet = new TH1D("nbJets", "nbJets", 14, 0, 14); 
  //m_hist_nbjet->Sumw2();
  //m_hist_btagSF = new TH1D("btagSF", "btagSF", 200, 0, 40); 
  //m_hist_btagSF->Sumw2();

  m_hist_jet_pT = new TH1D("jet_pT", "jet_pT", 200, 0, 200); 
  m_hist_jet_pT->Sumw2();
  m_hist_leading_jet_pT = new TH1D("leading_jet_pT", "leading_jet_pT", 200, 0, 200);
  m_hist_leading_jet_pT->Sumw2();

  m_hist_jet_e = new TH1D("jet_e", "jet_e", 200, 0, 200); 
  m_hist_jet_e->Sumw2();

  m_hist_jet_phi = new TH1D("jet_phi", "jet_phi", 80, -4, 4); 
  m_hist_jet_phi->Sumw2();

  m_hist_jet_eta = new TH1D("jet_eta", "jet_eta", 80, -4, 4); 
  m_hist_jet_eta->Sumw2();

  // Met kinematics(weighted)
  m_hist_MET_met = new TH1D("MET", "MET", 200, 0, 200);
  m_hist_MET_met->Sumw2();

  // Z(ee, mumu, emu) boson (weighted)
  m_hist_Z_pT_weighted = new TH1D("Z_pT_weighted", "Z_pT_weighted", 300, 0, 300);
  m_hist_Z_pT_weighted->Sumw2();

  m_hist_Z_eta_weighted = new TH1D("Z_eta_weighted", "Z_eta_weighted", 200, -10, 10);
  m_hist_Z_eta_weighted->Sumw2();

  m_hist_Z_phi_weighted = new TH1D("Z_phi_weighted", "Z_phi_weighted", 80, -4, 4);
  m_hist_Z_phi_weighted->Sumw2();

  m_hist_Z_e_weighted = new TH1D("Z_e_weighted", "Z_e_weighted", 400, 0, 800);
  m_hist_Z_e_weighted->Sumw2();

  m_hist_Z_mass_weighted = new TH1D("Z_mass_weighted", "Z_mass_weighted", 600, 0, 300);
  m_hist_Z_mass_weighted->Sumw2();

  // cut optimization
  m_hist_cut_optimization = new TH2F("cut_optimization","cut_optimization",100,0,100,100,0,100);



  wk()->addOutput(m_hist_1lep_CutFlow[0]);
  wk()->addOutput(m_hist_1lep_CutFlow_noWght[0]);
  wk()->addOutput(m_hist_1lep_CutFlow[1]);
  wk()->addOutput(m_hist_1lep_CutFlow_noWght[1]);
  wk()->addOutput(m_hist_1lep_CutFlow[2]);
  wk()->addOutput(m_hist_1lep_CutFlow_noWght[2]);

  // my own cutflow histograms
  wk()->addOutput(m_hist_2lep_CutFlow_noWght);
  wk()->addOutput(m_hist_2lep_CutFlow);
  wk()->addOutput(m_hist_el_CutFlow_noWght);
  wk()->addOutput(m_hist_mu_CutFlow_noWght);
  wk()->addOutput(m_hist_jet_CutFlow_noWght);
  wk()->addOutput(m_hist_el_CutFlow_withWght);
  wk()->addOutput(m_hist_mu_CutFlow_withWght);
  wk()->addOutput(m_hist_jet_CutFlow_withWght);


  wk()->addOutput(m_hist_averageInteractionsPerCrossing);
  wk()->addOutput(m_hist_averageInteractionsPerCrossingRecalc);
  wk()->addOutput(m_hist_Pileupweight);
  wk()->addOutput(m_hist_PileupweightRecalc);

  // event info
  wk()->addOutput(m_hist_EventWeight);
  wk()->addOutput(m_hist_MCEventWeight);
  wk()->addOutput(m_hist_LumiWeight);
  wk()->addOutput(m_hist_PileupWeight);
  wk()->addOutput(m_hist_averageInteractionsPerCrossingRecalc_afterCuts);
  wk()->addOutput(m_hist_leptonSF_el);
  wk()->addOutput(m_hist_leptonSF_el);
  // electron kinematics(weighted)
  wk()->addOutput(m_hist_nElectrons);
  wk()->addOutput(m_hist_electron_pT);
  wk()->addOutput(m_hist_electron_e);
  wk()->addOutput(m_hist_electron_phi);
  wk()->addOutput(m_hist_electron_eta);
  // muon kinematics(weighted)
  wk()->addOutput(m_hist_nMuons);
  wk()->addOutput(m_hist_muon_pT);
  wk()->addOutput(m_hist_muon_e);
  wk()->addOutput(m_hist_muon_phi);
  wk()->addOutput(m_hist_muon_eta);
  // jet kinematics(weighted)
  wk()->addOutput(m_hist_nJets);
  //wk()->addOutput(m_hist_nbjet);
  //wk()->addOutput(m_hist_btagSF);
  wk()->addOutput(m_hist_jet_pT);
  wk()->addOutput(m_hist_leading_jet_pT);
  wk()->addOutput(m_hist_jet_e);
  wk()->addOutput(m_hist_jet_phi);
  wk()->addOutput(m_hist_jet_eta);
  // Met kinematics(weighted)
  wk()->addOutput(m_hist_MET_met);
  // Z(ee, mumu, emu) boson (weighted)
  wk()->addOutput(m_hist_Z_pT_weighted);
  wk()->addOutput(m_hist_Z_eta_weighted);
  wk()->addOutput(m_hist_Z_phi_weighted);
  wk()->addOutput(m_hist_Z_e_weighted);
  wk()->addOutput(m_hist_Z_mass_weighted);

  wk()->addOutput(m_hist_cut_optimization);

  return EL::StatusCode::SUCCESS;
}




/**
 * This function fills all of the histograms required by the Z -> e mu
 * analysis.
 */
EL::StatusCode AnalysisReader_Zemu :: fill_Zemu(){
    if(m_debug) Info("fill_Zemu()", "Entered fill_Zemu() function");
    m_tree->Reset(); //Zero out all branches of the TTree

	//printf("in the Default Reader m_isMC: %s,  m_debug: %i \n", m_isMC? "true" : "false", m_mcChannel);

	// this is the place to set the variation to the tree. 
	// Becasue if SetVariation is set inside the MVATree_Zemu.cxx, 
	// m_currentVar will always be the last variation in the variations loop in MVATree_Zemu.cxx
	//std::cout << "inside AnalysisReader_Zemu :: fill_Zemu(), m_currentVar:   " << m_currentVar  << "\n" << std::endl;
    //m_tree->SetVariation(m_currentVar);
    //std::cout << "AnalysisReader_Zemu m_weight: " << m_weight << std::endl;
    //m_weight = 1.0; //What value would m_weight have before this?
	//m_weight is already decorated in the AnalysisReader::setEventWeight() function: MCEventWeight, applyLumiWeight, applyPUWeight


    //std::cout << "AnalysisReader_Zemu m_currentVar: " << m_currentVar << std::endl;

    //eventinfo variations?
    const xAOD::EventInfo* eventInfo = m_eventInfoReader->getObjects("Nominal");
    bool cutflow = m_currentVar=="Nominal" ;
    m_tree->EventNumber = eventInfo->eventNumber();

	if(eventInfo->eventNumber() == 4512889) return EL::StatusCode::SUCCESS;

    //std::cout << "#" << eventInfo->eventNumber() << " : pu=" << Props::PileupweightRecalc.get(eventInfo) << " : mc=" << Props::MCEventWeight.get(eventInfo) << std::endl;

    m_tree->mu = Props::averageInteractionsPerCrossingRecalc.get(eventInfo);
    m_hist_averageInteractionsPerCrossing->Fill(Props::averageInteractionsPerCrossing.get(eventInfo), Props::Pileupweight.get(eventInfo));
	m_hist_averageInteractionsPerCrossingRecalc->Fill(Props::averageInteractionsPerCrossingRecalc.get(eventInfo), Props::PileupweightRecalc.get(eventInfo));
	m_hist_Pileupweight->Fill(Props::Pileupweight.get(eventInfo));	
	m_hist_PileupweightRecalc->Fill(Props::PileupweightRecalc.get(eventInfo));	

 
    // Retrieve All Wanted Objects for this event
	// here all the objects passed the EventSelection_Zemu::passSelection() function
	// Becasue the m_eventSelection is already reassigned to EventSelection_Zemu before the filling in the default AnalysisReader::execute() function
	// here all the objects(el, mu, met, jets) of selectionResult should from struct ZemuResult, to which the container in execute() are passed.
    ZemuResult selectionResult = ((EventSelection_Zemu*)m_eventSelection)->m_result;
        std::vector<const xAOD::Electron*> electron = selectionResult.el;
        std::vector<const xAOD::Muon*> muon = selectionResult.mu;
        const xAOD::MissingET* met = selectionResult.met;
        std::vector<const xAOD::Jet*> jets = selectionResult.jets;
        std::vector<const xAOD::TruthParticle*> truthParticles = selectionResult.truthParticles;

    // partial pileup reweighting; don't forget to comment out the PU in CxAODReader/Root/AnalysisReader.cxx
    //bool m_applyPUWeight = true;
    //m_config->getif<bool>("applyPUWeight", m_applyPUWeight);
    //if(m_applyPUWeight && m_isMC) m_weight *= Props::PileupweightRecalc.get(eventInfo); //only apply the pileup weight on MC
    //double leptonSF = 1.;
    //if (m_isMC) leptonSF = Props::leptonSF.get(m_eventInfo);
        //m_weight *= leptonSF;

    if(cutflow) {
        m_hist_1lep_CutFlow[0]->Fill(m_hist_1lep_CutFlow[0]->GetXaxis()->FindBin("All"),m_weight);
        m_hist_1lep_CutFlow_noWght[0]->Fill(m_hist_1lep_CutFlow_noWght[0]->GetXaxis()->FindBin("All"));
        m_hist_1lep_CutFlow[1]->Fill(m_hist_1lep_CutFlow[1]->GetXaxis()->FindBin("All"),m_weight);
        m_hist_1lep_CutFlow_noWght[1]->Fill(m_hist_1lep_CutFlow_noWght[1]->GetXaxis()->FindBin("All"));
        m_hist_1lep_CutFlow[2]->Fill(m_hist_1lep_CutFlow[2]->GetXaxis()->FindBin("All"),m_weight);
        m_hist_1lep_CutFlow_noWght[2]->Fill(m_hist_1lep_CutFlow_noWght[2]->GetXaxis()->FindBin("All"));

		m_hist_2lep_CutFlow_noWght->Fill(m_hist_2lep_CutFlow_noWght->GetXaxis()->FindBin("All"));
		m_hist_2lep_CutFlow->Fill(m_hist_2lep_CutFlow->GetXaxis()->FindBin("All"), m_weight);
        m_hist_el_CutFlow_noWght->Fill(m_hist_el_CutFlow_noWght->GetXaxis()->FindBin("All"), electron.size());
        m_hist_mu_CutFlow_noWght->Fill(m_hist_mu_CutFlow_noWght->GetXaxis()->FindBin("All"), muon.size());
        m_hist_jet_CutFlow_noWght->Fill(m_hist_jet_CutFlow_noWght->GetXaxis()->FindBin("All"), jets.size());
        m_hist_el_CutFlow_withWght->Fill(m_hist_el_CutFlow_withWght->GetXaxis()->FindBin("All"), electron.size()*m_weight);
        m_hist_mu_CutFlow_withWght->Fill(m_hist_mu_CutFlow_withWght->GetXaxis()->FindBin("All"), muon.size()*m_weight);
        m_hist_jet_CutFlow_withWght->Fill(m_hist_jet_CutFlow_withWght->GetXaxis()->FindBin("All"), jets.size()*m_weight);
 
    }//end if - tell each cutflow the total number of events [entry 0]



    /** Check Event Triggers **/
	/*
    //int m_passHLT_e24_lhvloose_L1EM20VH = (m_isMC==0) ? Props::passHLT_e24_lhvloose_L1EM18VH.get(eventInfo)
    //                                                : Props::passHLT_e24_lhvloose_L1EM20VH.get(eventInfo);
    int m_passHLT_e24_lhmedium_L1EM20VH = (m_isMC==0) ? Props::passHLT_e24_lhmedium_L1EM20VH.get(eventInfo)
                                                    : Props::passHLT_e24_lhmedium_L1EM18VH.get(eventInfo) ;
    int m_passHLT_e60_lhmedium = Props::passHLT_e60_lhmedium.get(eventInfo);
    //int m_passHLT_e120_lhloose = Props::passHLT_e120_lhloose.get(eventInfo);
    int m_passHLT_mu20_iloose_L1MU15 = Props::passHLT_mu20_iloose_L1MU15.get(eventInfo);
    int m_passHLT_mu50 = Props::passHLT_mu50.get(eventInfo);
    //int m_passHLT_xe70 = Props::passHLT_xe70.get(eventInfo); //MET Trigger

    //int pass_Trigger = m_passHLT_e24_lhvloose_L1EM20VH || m_passHLT_e24_lhmedium_L1EM20VH || m_passHLT_e60_lhmedium ||
    //                    m_passHLT_mu20_iloose_L1MU15 || m_passHLT_mu50;
	
	int pass_Trigger = 1;
    if(!pass_Trigger){
        if(m_debug) Info("fill_Zemu()","Failed event trigger");
        return EL::StatusCode::SUCCESS;
    }//end if : check if any trigger was passed.
	*/


	// the single-lepton trigger pass(electron)
	// here if it's MC, we use passHLT_e24_lhmedium_L1EM18VH instead of passHLT_e24_lhmedium_L1EM20VH, because of the threshold change of mc and data.
	// 2015
    int m_passHLT_e24_lhmedium_L1EM20VH = (m_isMC==0) ? Props::passHLT_e24_lhmedium_L1EM20VH.get(eventInfo)
                                                    : Props::passHLT_e24_lhmedium_L1EM18VH.get(eventInfo) ;
    int m_passHLT_e60_lhmedium = Props::passHLT_e60_lhmedium.get(eventInfo);
    int m_passHLT_e120_lhloose = Props::passHLT_e120_lhloose.get(eventInfo);

	// the single-lepton trigger pass(muon)
	int m_passHLT_mu20_iloose_L1MU15 = Props::passHLT_mu20_iloose_L1MU15.get(eventInfo);
	//int m_passHLT_mu40 = Props::passHLT_mu40.get(eventInfo);  // here for MC the passHLT_mu40 is empty, so we use mu50 instead
	int m_passHLT_mu50 = Props::passHLT_mu50.get(eventInfo);

	// 2016
	// single electron
	int m_passHLT_e24_lhtight_nod0_ivarloose = Props::passHLT_e24_lhtight_nod0_ivarloose.get(eventInfo);
	int m_passHLT_e60_lhmedium_nod0 = Props::passHLT_e60_lhmedium_nod0.get(eventInfo);
	int m_passHLT_e60_medium = Props::passHLT_e60_medium.get(eventInfo);
	int m_passHLT_e140_lhloose_nod0 = Props::passHLT_e140_lhloose_nod0.get(eventInfo);
	int m_passHLT_e300_etcut = Props::passHLT_e300_etcut.get(eventInfo);
	int m_passHLT_e26_lhtight_nod0_ivarloose = Props::passHLT_e26_lhtight_nod0_ivarloose.get(eventInfo);

	// single muon
	int m_passHLT_mu24_iloose = (m_isMC==0) ? Props::passHLT_mu24_iloose.get(eventInfo) 
											: Props::passHLT_mu24_iloose_L1MU15.get(eventInfo);
	int m_passHLT_mu24_ivarloose = (m_isMC==0) ? Props::passHLT_mu24_ivarloose.get(eventInfo)
												: Props::passHLT_mu24_ivarloose_L1MU15.get(eventInfo);
	int m_passHLT_mu40 = Props::passHLT_mu40.get(eventInfo);	
	int m_passHLT_mu24_ivarmedium = Props::passHLT_mu24_ivarmedium.get(eventInfo);
	int m_passHLT_mu24_imedium = Props::passHLT_mu24_imedium.get(eventInfo);
	int m_passHLT_mu26_ivarmedium = Props::passHLT_mu26_ivarmedium.get(eventInfo);
	int m_passHLT_mu26_imedium = Props::passHLT_mu26_imedium.get(eventInfo);


    bool electronTriggerPass = m_passHLT_e24_lhmedium_L1EM20VH || m_passHLT_e60_lhmedium || m_passHLT_e120_lhloose
							|| m_passHLT_e24_lhtight_nod0_ivarloose || m_passHLT_e60_lhmedium_nod0 || m_passHLT_e60_medium 
							|| m_passHLT_e140_lhloose_nod0 || m_passHLT_e300_etcut || m_passHLT_e26_lhtight_nod0_ivarloose;
    bool muonTriggerPass = m_passHLT_mu20_iloose_L1MU15 || m_passHLT_mu50
						|| m_passHLT_mu24_iloose || m_passHLT_mu24_ivarloose || m_passHLT_mu40 || m_passHLT_mu24_ivarmedium
						|| m_passHLT_mu24_imedium || m_passHLT_mu26_ivarmedium || m_passHLT_mu26_imedium;

    if(!(( electronTriggerPass ) || ( muonTriggerPass )) ){
        if(m_debug) Info("fill_Zemu()","Failed El or Mu passTrigger");
        return EL::StatusCode::SUCCESS;
    }//end if : require corresponding event trigger match later
    if(cutflow) {
        m_hist_1lep_CutFlow[0]->Fill(m_hist_1lep_CutFlow[0]->GetXaxis()->FindBin("Event Trigger"),m_weight);
        m_hist_1lep_CutFlow_noWght[0]->Fill(m_hist_1lep_CutFlow_noWght[0]->GetXaxis()->FindBin("Event Trigger"));
        m_hist_1lep_CutFlow[1]->Fill(m_hist_1lep_CutFlow[1]->GetXaxis()->FindBin("Event Trigger"),m_weight);
        m_hist_1lep_CutFlow_noWght[1]->Fill(m_hist_1lep_CutFlow_noWght[1]->GetXaxis()->FindBin("Event Trigger"));
        m_hist_1lep_CutFlow[2]->Fill(m_hist_1lep_CutFlow[2]->GetXaxis()->FindBin("Event Trigger"),m_weight);
        m_hist_1lep_CutFlow_noWght[2]->Fill(m_hist_1lep_CutFlow_noWght[2]->GetXaxis()->FindBin("Event Trigger"));

        m_hist_2lep_CutFlow_noWght->Fill(m_hist_2lep_CutFlow_noWght->GetXaxis()->FindBin("passTrigger"));
        m_hist_2lep_CutFlow->Fill(m_hist_2lep_CutFlow->GetXaxis()->FindBin("passTrigger"), m_weight);
		m_hist_el_CutFlow_noWght->Fill(m_hist_el_CutFlow_noWght->GetXaxis()->FindBin("passTrigger"), electron.size());
		m_hist_mu_CutFlow_noWght->Fill(m_hist_mu_CutFlow_noWght->GetXaxis()->FindBin("passTrigger"), muon.size());
		m_hist_jet_CutFlow_noWght->Fill(m_hist_jet_CutFlow_noWght->GetXaxis()->FindBin("passTrigger"), jets.size());
		m_hist_el_CutFlow_withWght->Fill(m_hist_el_CutFlow_withWght->GetXaxis()->FindBin("passTrigger"), electron.size()*m_weight);
		m_hist_mu_CutFlow_withWght->Fill(m_hist_mu_CutFlow_withWght->GetXaxis()->FindBin("passTrigger"), muon.size()*m_weight);
		m_hist_jet_CutFlow_withWght->Fill(m_hist_jet_CutFlow_withWght->GetXaxis()->FindBin("passTrigger"), jets.size()*m_weight);
    
    }//end if - tell each cutflow the total number of events [entry 0]

	// primary vertex
	if( !Props::hasPV.get(eventInfo)) return EL::StatusCode::SUCCESS;
	if(cutflow) {
		m_hist_2lep_CutFlow_noWght->Fill(m_hist_2lep_CutFlow_noWght->GetXaxis()->FindBin("PrimaryVertex"));
		m_hist_2lep_CutFlow->Fill(m_hist_2lep_CutFlow->GetXaxis()->FindBin("PrimaryVertex"), m_weight);
		m_hist_el_CutFlow_noWght->Fill(m_hist_el_CutFlow_noWght->GetXaxis()->FindBin("PrimaryVertex"), electron.size());
		m_hist_mu_CutFlow_noWght->Fill(m_hist_mu_CutFlow_noWght->GetXaxis()->FindBin("PrimaryVertex"), muon.size());
		m_hist_jet_CutFlow_noWght->Fill(m_hist_jet_CutFlow_noWght->GetXaxis()->FindBin("PrimaryVertex"), jets.size());
		m_hist_el_CutFlow_withWght->Fill(m_hist_el_CutFlow_withWght->GetXaxis()->FindBin("PrimaryVertex"), electron.size()*m_weight);
		m_hist_mu_CutFlow_withWght->Fill(m_hist_mu_CutFlow_withWght->GetXaxis()->FindBin("PrimaryVertex"), muon.size()*m_weight);
		m_hist_jet_CutFlow_withWght->Fill(m_hist_jet_CutFlow_withWght->GetXaxis()->FindBin("PrimaryVertex"), jets.size()*m_weight);
	
	}


    /** Check Leptons **/
    if( m_debug ) Info("fill_Zemu()", "Checking Electrons");
	std::vector<const xAOD::Electron*> el_good;
	std::vector<const xAOD::Muon*> mu_good;
    el_good.clear();
    mu_good.clear();
    int nelecs_loose = 0;
    int nelecs_good = 0;
    int nmuons_loose = 0;
    int nmuons_good = 0;
	std::vector<int> charge_el;
	charge_el.clear();
	std::vector<int> charge_mu;
	charge_mu.clear();

    TLorentzVector lepVec_el; // be used for electrons in its loop
	int m_matchHLT_e24_lhmedium_L1EM20VH = 0;
	int m_matchHLT_e60_lhmedium = 0;
	int m_matchHLT_e120_lhloose = 0;

	int m_matchHLT_e24_lhtight_nod0_ivarloose = 0;
	int m_matchHLT_e60_lhmedium_nod0 = 0;
	int m_matchHLT_e60_medium = 0;		
	int m_matchHLT_e140_lhloose_nod0 = 0;
	int m_matchHLT_e300_etcut = 0;
	int m_matchHLT_e26_lhtight_nod0_ivarloose = 0;

	int electronTriggerMatch = 0;
	if( m_debug ) Info("fill_Zemu()", "Checking Electrons");
	for(unsigned int i = 0; i < electron.size(); i++){

		lepVec_el=electron[i]->p4();
		// 2015
		m_matchHLT_e24_lhmedium_L1EM20VH = (m_isMC==0) ? Props::matchHLT_e24_lhmedium_L1EM20VH.get(electron[i])
														:Props::matchHLT_e24_lhmedium_L1EM18VH.get(electron[i]);
		m_matchHLT_e60_lhmedium = Props::matchHLT_e60_lhmedium.get(electron[i]);
		m_matchHLT_e120_lhloose = Props::matchHLT_e120_lhloose.get(electron[i]);
		// 2016
	    m_matchHLT_e24_lhtight_nod0_ivarloose = Props::matchHLT_e24_lhtight_nod0_ivarloose.get(electron[i]);
    	m_matchHLT_e60_lhmedium_nod0 = Props::matchHLT_e60_lhmedium_nod0.get(electron[i]);
    	m_matchHLT_e60_medium = Props::matchHLT_e60_medium.get(electron[i]);
    	m_matchHLT_e140_lhloose_nod0 = Props::matchHLT_e140_lhloose_nod0.get(electron[i]);
    	m_matchHLT_e300_etcut = Props::matchHLT_e300_etcut.get(electron[i]);
    	m_matchHLT_e26_lhtight_nod0_ivarloose = Props::matchHLT_e26_lhtight_nod0_ivarloose.get(electron[i]);	

		electronTriggerMatch += (m_passHLT_e24_lhmedium_L1EM20VH && m_matchHLT_e24_lhmedium_L1EM20VH) 
							|| (m_passHLT_e60_lhmedium && m_matchHLT_e60_lhmedium) 
							|| (m_passHLT_e120_lhloose && m_matchHLT_e120_lhloose)
							|| (m_passHLT_e24_lhtight_nod0_ivarloose && m_matchHLT_e24_lhtight_nod0_ivarloose)
							|| (m_passHLT_e60_lhmedium_nod0 && m_matchHLT_e60_lhmedium_nod0)
							|| (m_passHLT_e60_medium && m_matchHLT_e60_medium)
							|| (m_passHLT_e140_lhloose_nod0 && m_matchHLT_e140_lhloose_nod0)
							|| (m_passHLT_e300_etcut && m_matchHLT_e300_etcut)
							|| (m_passHLT_e26_lhtight_nod0_ivarloose && m_matchHLT_e26_lhtight_nod0_ivarloose);

	
		// all the cuts on electron object
		//if(!(electron[i]->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON))) continue; // isGoodOQ, but not exist, might need to make it in Maker
		if(!Props::isGoodOQ.get(electron[i])) continue; 
        if(cutflow) m_hist_el_CutFlow_noWght->Fill(m_hist_el_CutFlow_noWght->GetXaxis()->FindBin("isGoodOQ"));
        if(cutflow) m_hist_el_CutFlow_withWght->Fill(m_hist_el_CutFlow_withWght->GetXaxis()->FindBin("isGoodOQ"), m_weight);

		if(lepVec_el.Pt()/GeV <= 27) continue; // might use 27GeV in 2016???
		if(cutflow) m_hist_el_CutFlow_noWght->Fill(m_hist_el_CutFlow_noWght->GetXaxis()->FindBin("pt>27"));
		if(cutflow) m_hist_el_CutFlow_withWght->Fill(m_hist_el_CutFlow_withWght->GetXaxis()->FindBin("pt>27"), m_weight);

		if(fabs(lepVec_el.Eta())>2.47 || (fabs(lepVec_el.Eta())>1.37 && fabs(lepVec_el.Eta())<1.52)) continue;
        if(cutflow) m_hist_el_CutFlow_noWght->Fill(m_hist_el_CutFlow_noWght->GetXaxis()->FindBin("|eta|<2.47 and 1.37,1.52"));
        if(cutflow) m_hist_el_CutFlow_withWght->Fill(m_hist_el_CutFlow_withWght->GetXaxis()->FindBin("|eta|<2.47 and 1.37,1.52"), m_weight);

		if(fabs(Props::z0sinTheta.get(electron[i])) >= 0.5) continue; // default use z0sinTheta, which is z0*sinTheta
        if(cutflow) m_hist_el_CutFlow_noWght->Fill(m_hist_el_CutFlow_noWght->GetXaxis()->FindBin("|z0sinTheta|<0.5"));
        if(cutflow) m_hist_el_CutFlow_withWght->Fill(m_hist_el_CutFlow_withWght->GetXaxis()->FindBin("|z0sinTheta|<0.5"), m_weight);

		if(fabs(Props::d0sigBL.get(electron[i])) >= 5.0) continue; // default use d0sigBL, in the paper is should less than 6 ?
        if(cutflow) m_hist_el_CutFlow_noWght->Fill(m_hist_el_CutFlow_noWght->GetXaxis()->FindBin("|d0_sig|<5"));
        if(cutflow) m_hist_el_CutFlow_withWght->Fill(m_hist_el_CutFlow_withWght->GetXaxis()->FindBin("|d0_sig|<5"), m_weight);

        if(Props::passOR_forReader.get(electron[i]) == 0) continue; // or should use Props::passORGlob?
        if(cutflow) m_hist_el_CutFlow_noWght->Fill(m_hist_el_CutFlow_noWght->GetXaxis()->FindBin("passOR"));
        if(cutflow) m_hist_el_CutFlow_withWght->Fill(m_hist_el_CutFlow_withWght->GetXaxis()->FindBin("passOR"), m_weight);

		// count loose electrons for veto
		if(Props::isLooseLH.get(electron[i]) == 0) continue;
        if(cutflow) m_hist_el_CutFlow_noWght->Fill(m_hist_el_CutFlow_noWght->GetXaxis()->FindBin("isLooseLH"));
        if(cutflow) m_hist_el_CutFlow_withWght->Fill(m_hist_el_CutFlow_withWght->GetXaxis()->FindBin("isLooseLH"), m_weight);
		nelecs_loose++;

		//if(Props::isMediumLH.get(electron[i]) == 0) continue;
		if(Props::isTightLH.get(electron[i]) == 0) continue;
        if(cutflow) m_hist_el_CutFlow_noWght->Fill(m_hist_el_CutFlow_noWght->GetXaxis()->FindBin("isMediumLH"));
        if(cutflow) m_hist_el_CutFlow_withWght->Fill(m_hist_el_CutFlow_withWght->GetXaxis()->FindBin("isMediumLH"), m_weight);

		if(Props::isGradientIso.get(electron[i]) == 0) continue; 
        if(cutflow) m_hist_el_CutFlow_noWght->Fill(m_hist_el_CutFlow_noWght->GetXaxis()->FindBin("isGradientIso"));
        if(cutflow) m_hist_el_CutFlow_withWght->Fill(m_hist_el_CutFlow_withWght->GetXaxis()->FindBin("isGradientIso"), m_weight);

		el_good.push_back(electron[i]);
		nelecs_good++;
		
		// push electron info into the tree, but not fill yet
		
		m_tree->Leptons_nElectrons += 1;
		m_tree->Leptons_electron_pT.push_back(lepVec_el.Pt()/GeV);
		m_tree->Leptons_electron_eta.push_back(lepVec_el.Eta());
		m_tree->Leptons_electron_phi.push_back(lepVec_el.Phi());
		//m_tree->Leptons_electron_e.push_back(lepVec_el.E()/GeV);
		m_tree->Leptons_electron_charge.push_back(electron[i]->charge());

		//m_tree->Leptons_electron_d0.push_back(Props::d0.get(electron[i]));
		m_tree->Leptons_electron_d0sig.push_back(Props::d0sig.get(electron[i]));
		m_tree->Leptons_electron_z0.push_back(Props::z0.get(electron[i]));
		//m_tree->Leptons_electron_isGradientIso.push_back(Props::isGradientIso.get(electron[i]));	
		//m_tree->Leptons_electron_isGradientIso.push_back(Props::isGradientLooseIso.get(electron[i]));	
		
		charge_el.push_back(electron[i]->charge());

        int quality = 0;
            if( Props::isLooseLH.get(electron[i]) ) quality = 1;
            if( Props::isMediumLH.get(electron[i]) ) quality = 2;
            if( Props::isTightLH.get(electron[i]) ) quality = 3;
        //m_tree->Leptons_electron_quality.push_back(quality);

		// fill electron efficiency scale factors(recostruction, isolation, identification, trigger efficiencies)
		/*	
		if( m_isMC ){
			m_tree->Leptons_electron_effSFReco.push_back(Props::effSFReco.get(electron[i]));
        	m_tree->Leptons_electron_effSFIsoGradientMediumLH.push_back(Props::effSFIsoGradientMediumLH.get(electron[i]));
        	m_tree->Leptons_electron_effSFmediumLH.push_back(Props::effSFmediumLH.get(electron[i]));
			m_tree->Leptons_electron_trigSFmediumLHIsoGradient.push_back(Props::trigSFmediumLHIsoGradient.get(electron[i]));
		}
		*/
	}//end for : Loop over all electrons
	lepVec_el.Delete();
	if(nelecs_good > 1){ 
    	if(cutflow) m_hist_el_CutFlow_noWght->Fill(m_hist_el_CutFlow_noWght->GetXaxis()->FindBin(">1 el")); // the cutflow for number of good electrons
    	if(cutflow) m_hist_el_CutFlow_withWght->Fill(m_hist_el_CutFlow_withWght->GetXaxis()->FindBin(">1 el"), m_weight); // the cutflow for number of good electrons
	}

    TLorentzVector lepVec_mu; // be used for muons in its loop
	int m_matchHLT_mu20_iloose_L1MU15 = 0;
	//int m_matchHLT_mu40 = 0;
	int m_matchHLT_mu50 = 0;
    int muonTriggerMatch = 0;
	int m_matchHLT_mu24_iloose = 0;
	int m_matchHLT_mu24_ivarloose = 0;
	int m_matchHLT_mu40 = 0;
	int m_matchHLT_mu24_ivarmedium =0;
	int m_matchHLT_mu24_imedium = 0;
	int m_matchHLT_mu26_ivarmedium = 0;
	int m_matchHLT_mu26_imedium = 0;


    if( m_debug ) Info("fill_Zemu()", "Checking Muons.");
    for(unsigned int i = 0; i < muon.size(); i++){

        lepVec_mu=muon[i]->p4();
		// 2015
		m_matchHLT_mu20_iloose_L1MU15 = Props::matchHLT_mu20_iloose_L1MU15.get(muon[i]);
		//m_matchHLT_mu40 = Props::matchHLT_mu40.get(muon[i]);
		m_matchHLT_mu50 = Props::matchHLT_mu50.get(muon[i]);
		// 2016
	    m_matchHLT_mu24_iloose = (m_isMC==0) ? Props::matchHLT_mu24_iloose.get(muon[i])
                                        : Props::matchHLT_mu24_iloose_L1MU15.get(muon[i]);
  		m_matchHLT_mu24_ivarloose = (m_isMC==0) ? Props::matchHLT_mu24_ivarloose.get(muon[i])
                                            : Props::matchHLT_mu24_ivarloose_L1MU15.get(muon[i]);
    	m_matchHLT_mu40 = Props::matchHLT_mu40.get(muon[i]);
    	m_matchHLT_mu24_ivarmedium = Props::matchHLT_mu24_ivarmedium.get(muon[i]);
    	m_matchHLT_mu24_imedium = Props::matchHLT_mu24_imedium.get(muon[i]);
    	m_matchHLT_mu26_ivarmedium = Props::matchHLT_mu26_ivarmedium.get(muon[i]);
    	m_matchHLT_mu26_imedium = Props::matchHLT_mu26_imedium.get(muon[i]);


		//muonTriggerMatch = (m_passHLT_mu20_iloose_L1MU15 && m_matchHLT_mu20_iloose_L1MU15) || (m_passHLT_mu40 && m_matchHLT_mu40); 
		muonTriggerMatch += (m_passHLT_mu20_iloose_L1MU15 && m_matchHLT_mu20_iloose_L1MU15) 
						|| (m_passHLT_mu50 && m_matchHLT_mu50)
						|| (m_passHLT_mu24_iloose && m_matchHLT_mu24_iloose)
						|| (m_passHLT_mu24_ivarloose && m_matchHLT_mu24_ivarloose)
						|| (m_passHLT_mu40 && m_matchHLT_mu40)
						|| (m_passHLT_mu24_ivarmedium && m_matchHLT_mu24_ivarmedium)
						|| (m_passHLT_mu24_imedium && m_matchHLT_mu24_imedium)
						|| (m_passHLT_mu26_ivarmedium || m_matchHLT_mu26_ivarmedium)
						|| (m_passHLT_mu26_imedium && m_matchHLT_mu26_imedium); 

		// all the cuts on the muon object
        if(lepVec_mu.Pt()/GeV <= 27) continue; // might use 27GeV in 2016??? because we need to above the threshold of muon trigger to be safe
		if(cutflow) m_hist_mu_CutFlow_noWght->Fill(m_hist_mu_CutFlow_noWght->GetXaxis()->FindBin("pt>27"));
		if(cutflow) m_hist_mu_CutFlow_withWght->Fill(m_hist_mu_CutFlow_withWght->GetXaxis()->FindBin("pt>27"), m_weight);

		// muon spectrometer track must match the inner detector track(combined muons)
		// muonType()==0 is Combined, muonType()==1 is MuonStandAlone, muonType()>1 is Segment, Calo, Silicon 
		if(muon[i]->muonType() != 0) continue; 
		if(cutflow) m_hist_mu_CutFlow_noWght->Fill(m_hist_mu_CutFlow_noWght->GetXaxis()->FindBin("Combined Muon"));
		if(cutflow) m_hist_mu_CutFlow_withWght->Fill(m_hist_mu_CutFlow_withWght->GetXaxis()->FindBin("Combined Muon"), m_weight);

        if(Props::muonQuality.get(muon[i]) >= 2) continue; // here 0 is tight, 1 is medium, 2 is loose. and here we choose tight and medium 
        if(cutflow) m_hist_mu_CutFlow_noWght->Fill(m_hist_mu_CutFlow_noWght->GetXaxis()->FindBin("muon Quality"));
        if(cutflow) m_hist_mu_CutFlow_withWght->Fill(m_hist_mu_CutFlow_withWght->GetXaxis()->FindBin("muon Quality"), m_weight);

		if(fabs(lepVec_mu.Eta()) >= 2.5) continue;
        if(cutflow) m_hist_mu_CutFlow_noWght->Fill(m_hist_mu_CutFlow_noWght->GetXaxis()->FindBin("|eta|<2.5"));
        if(cutflow) m_hist_mu_CutFlow_withWght->Fill(m_hist_mu_CutFlow_withWght->GetXaxis()->FindBin("|eta|<2.5"), m_weight);

		if(fabs(Props::z0sinTheta.get(muon[i])) >= 0.5) continue; // default use z0sinTheta, which is z0*sinTheta
        if(cutflow) m_hist_mu_CutFlow_noWght->Fill(m_hist_mu_CutFlow_noWght->GetXaxis()->FindBin("|z0sinTheta|<0.5"));
        if(cutflow) m_hist_mu_CutFlow_withWght->Fill(m_hist_mu_CutFlow_withWght->GetXaxis()->FindBin("|z0sinTheta|<0.5"), m_weight);

		if(fabs(Props::d0sigBL.get(muon[i])) >= 3.0) continue; // default use d0sigBL, and it's 3 in the run1 note
        if(cutflow) m_hist_mu_CutFlow_noWght->Fill(m_hist_mu_CutFlow_noWght->GetXaxis()->FindBin("|d0_sig|<3"));
        if(cutflow) m_hist_mu_CutFlow_withWght->Fill(m_hist_mu_CutFlow_withWght->GetXaxis()->FindBin("|d0_sig|<3"), m_weight);

        if(Props::passOR_forReader.get(muon[i]) == 0) continue; 
        if(cutflow) m_hist_mu_CutFlow_noWght->Fill(m_hist_mu_CutFlow_noWght->GetXaxis()->FindBin("passOR"));
        if(cutflow) m_hist_mu_CutFlow_withWght->Fill(m_hist_mu_CutFlow_withWght->GetXaxis()->FindBin("passOR"), m_weight);
	
		// count loose muons for veto
        if(Props::isGradientLooseIso.get(muon[i]) == 0) continue;
        if(cutflow) m_hist_mu_CutFlow_noWght->Fill(m_hist_mu_CutFlow_noWght->GetXaxis()->FindBin("isGradientLooseIso"));
        if(cutflow) m_hist_mu_CutFlow_withWght->Fill(m_hist_mu_CutFlow_withWght->GetXaxis()->FindBin("isGradientLooseIso"), m_weight);
        nmuons_loose++;

        if(Props::isGradientIso.get(muon[i]) == 0) continue; 
        if(cutflow) m_hist_mu_CutFlow_noWght->Fill(m_hist_mu_CutFlow_noWght->GetXaxis()->FindBin("isGradientIso"));
        if(cutflow) m_hist_mu_CutFlow_withWght->Fill(m_hist_mu_CutFlow_withWght->GetXaxis()->FindBin("isGradientIso"), m_weight);

        mu_good.push_back(muon[i]);
        nmuons_good++;

		// push muon info into the tree
		
        m_tree->Leptons_nMuons += 1;
        m_tree->Leptons_muon_pT.push_back(lepVec_mu.Pt()/GeV);
        m_tree->Leptons_muon_eta.push_back(lepVec_mu.Eta());
        m_tree->Leptons_muon_phi.push_back(lepVec_mu.Phi());
        //m_tree->Leptons_muon_e.push_back(lepVec_mu.E()/GeV);	
		m_tree->Leptons_muon_charge.push_back(muon[i]->charge());

	    //m_tree->Leptons_muon_d0.push_back(Props::d0.get(muon[i]));
    	m_tree->Leptons_muon_d0sig.push_back(Props::d0sig.get(muon[i]));
        m_tree->Leptons_muon_z0.push_back(Props::z0.get(muon[i]));
        //m_tree->Leptons_muon_isGradientIso.push_back(Props::isGradientIso.get(muon[i]));
        //m_tree->Leptons_muon_isGradientIso.push_back(Props::isLooseIso.get(muon[i]));
		
		charge_mu.push_back(muon[i]->charge());

        // fill muon efficiency scale factors(recostruction, isolation, identification, trigger)
		/*
		if( m_isMC ){
        	m_tree->Leptons_muon_TTVAEffSF.push_back(Props::TTVAEffSF.get(muon[i]));
        	//m_tree->Leptons_muon_effSF.push_back(Props::effSF.get(muon[i])); // it seems in the MuonHandler, effSF is looseEffSF
        	m_tree->Leptons_muon_gradientIsoSF.push_back(Props::gradientIsoSF.get(muon[i]));
        	m_tree->Leptons_muon_mediumEffSF.push_back(Props::mediumEffSF.get(muon[i]));
		}
		*/

    }//end for : Loop over all muons
	lepVec_mu.Delete();
	if(nmuons_good > 1){ 
    	if(cutflow) m_hist_mu_CutFlow_noWght->Fill(m_hist_mu_CutFlow_noWght->GetXaxis()->FindBin(">1 mu")); // the cutflow to see the number of good muons
    	if(cutflow) m_hist_mu_CutFlow_withWght->Fill(m_hist_mu_CutFlow_withWght->GetXaxis()->FindBin(">1 mu"), m_weight); // the cutflow to see the number of good muons
	}


    /** Check Jets **/
    std::vector<const xAOD::Jet*> jets_good;
    std::vector<const xAOD::Jet*> jets_btagged;
    jets_good.clear();
    jets_btagged.clear();
    TLorentzVector jetVec;
	std::vector<double> jet_JvtSF;
	jet_JvtSF.clear();

    int n_bjets = 0;

    compute_btagging(); // the input is "jet" which is from original AnalysisReader.cxx
						// if (m_jetReader) m_jets = m_jetReader->getObjects(varName);, not "jets" from the selectionResult.jets 


    if(m_debug) Info("fill_Zemu()", "Begin loop over jets");
    for(unsigned int iJet=0; iJet < jets.size(); iJet++){
        jetVec = jets[iJet]->p4();

        //Only keep this jet if it passes basic kinematic criteria
        //if( jetVec.Et() < 20*GeV || fabs(jetVec.Eta()) > 2.5 ) continue;
        if(Props::passOR_forReader.get(jets[iJet]) == 0) continue; 
        if(cutflow) m_hist_jet_CutFlow_noWght->Fill(m_hist_jet_CutFlow_noWght->GetXaxis()->FindBin("OverlapRemoval e/mu"));
        if(cutflow) m_hist_jet_CutFlow_withWght->Fill(m_hist_jet_CutFlow_withWght->GetXaxis()->FindBin("OverlapRemoval e/mu"), m_weight);

		if(jetVec.Pt()/GeV < 20.0) continue;
        if(fabs(jetVec.Eta()) >= 2.5) continue;
        if(cutflow) m_hist_jet_CutFlow_noWght->Fill(m_hist_jet_CutFlow_noWght->GetXaxis()->FindBin("|eta|<2.5"));
        if(cutflow) m_hist_jet_CutFlow_withWght->Fill(m_hist_jet_CutFlow_withWght->GetXaxis()->FindBin("|eta|<2.5"), m_weight);

		if( jetVec.Pt()/GeV < 60.0 && fabs(jetVec.Eta()) < 2.4 ){  // jet pt 60 is recommended now	
			if(Props::Jvt.get(jets[iJet]) <= 0.59) continue; 
			if( m_isMC ) jet_JvtSF.push_back(Props::JvtSF.get(jets[iJet])); // this is used as when the jvt cut is being used and should be applied to weight
		}
		// just for the Hemu channel
		//if(jetVec.Pt()/GeV < 25.0) continue; 
        if(cutflow) m_hist_jet_CutFlow_noWght->Fill(m_hist_jet_CutFlow_noWght->GetXaxis()->FindBin("Jvt>0.59"));
        if(cutflow) m_hist_jet_CutFlow_withWght->Fill(m_hist_jet_CutFlow_withWght->GetXaxis()->FindBin("Jvt>0.59"), m_weight); // apply the jet_JvtSF to the qualified jets?

        //Write the jet information to the tree.
		
        //m_tree->Jets_E.push_back(jetVec.E()/GeV);
        m_tree->Jets_pT.push_back(jetVec.Pt()/GeV);
        m_tree->Jets_eta.push_back(jetVec.Eta());
        m_tree->Jets_phi.push_back(jetVec.Phi());

        m_tree->Jets_isBTagged.push_back(BTagProps::isTagged.get(jets[iJet]));
		if(BTagProps::isTagged.get(jets[iJet])==1){ 
			m_tree->Jets_nbjet += 1;
			n_bjets++;
        	jets_btagged.push_back(jets[iJet]);
		}
        m_tree->Jets_mv2c10.push_back(Props::MV2c10.get(jets[iJet]));
        //m_tree->Jets_mv2c20.push_back(Props::MV2c20.get(jets[iJet]));


        //m_tree->Jets_Jvt.push_back(Props::Jvt.get(jets[iJet]));
        //if( m_isMC ) m_tree->Jets_JvtSF.push_back(Props::JvtSF.get(jets[iJet]));
		/*
        //m_tree->Jets_isClean.push_back(LLPProps::isCleanJet.get(jets[iJet]));
        //m_tree->Jets_timing.push_back(LLPProps::Timing.get(jets[iJet]));
        m_tree->Jets_nTrks.push_back(Props::NumTrkPt1000PV.get(jets[iJet]));
        m_tree->Jets_isBTagged.push_back(BTagProps::isTagged.get(jets[iJet]));


        //m_tree->Jets_chFrac.push_back(LLPProps::EMFrac.get(jets[iJet]));
        //m_tree->Jets_logRatio.push_back(getLogRatio(LLPProps::EMFrac.get(jets[iJet])));
        //bool isCr = isCRjet(jets[iJet]);
        //m_tree->Jets_isCR.push_back( isCr );
        //m_tree->Jets_width.push_back(LLPProps::Width.get(jets[iJet]));

        m_tree->Jets_mv2c10.push_back(Props::MV2c10.get(jets[iJet]));
        m_tree->Jets_mv2c20.push_back(Props::MV2c20.get(jets[iJet]));
		*/
        //if( isCr ) m_tree->Jets_nCRJets += 1;
        jets_good.push_back(jets[iJet]);
        m_tree->Jets_nJets += 1;
    }//end for : loop over jets
	jetVec.Delete();
	// to store the highest pt jet, but the first jet is the leading jet?
	// yes, inside the file the "sort_pt" is used: CxAODTools_Zemu/Root/EventSelection_Zemu.cxx
	TLorentzVector jetVec_leading;
	TLorentzVector jetVec_subleading;
	if(jets_good.size()!=0){ 
		jetVec_leading = jets_good.at(0)->p4();
    	m_tree->Jets_leading_pT=jetVec_leading.Pt()/GeV;
		if(jets_good.size()>1){
			jetVec_subleading = jets_good.at(1)->p4();
    		m_tree->Jets_subleading_pT=jetVec_subleading.Pt()/GeV;
		}
	}
    if(jets_good.size()==0){
        m_tree->Jets_leading_pT=0;
    }

    /** check MET **/
    //Add the MET to the event
	// is it requiring all the events has MET?
    if( !met ){ // here this !met way not useful to the vector<xAOD* XXX> 
        std::cerr << "MET POINTER NOT INITIALIZED!" << std::endl;
        return EL::StatusCode::SUCCESS;
    }//end if
    m_tree->MET_met = met->met()/GeV;
    //m_tree->MET_phi = met->phi();

	
	/*
	///////////// check truth particle ///////////////////
    std::vector<const xAOD::TruthParticle*> truthParticle_good;
	truthParticle_good.clear();
    TLorentzVector truthParticleVec;
    if(m_debug) Info("fill_Zemu()", "Begin loop over truth particles");
    for(unsigned int itruthParticle=0; itruthParticle < truthParticles.size(); itruthParticle++){
		truthParticleVec = truthParticles[itruthParticle]->p4();
		std::cout << "truthParticle Pt: " << truthParticleVec.Pt()/GeV << "\n";

	}
	truthParticleVec.Delete();
	*/

	
	//////////// Event cuts /////////////////

	//std::string signal_channel = "Zmumu";  // it can be Zemu, Zee, Zmumu
    m_config->getif<string>("signal_channel", m_signal_channel);

	// cuts on the number of qualified leptons
	if(m_signal_channel == "Zemu"){ // signal_channel.compare("Zemu") == 0 if std::string signal_channel = "Zemu"
		if( nelecs_good + nmuons_good < 1 ) return EL::StatusCode::SUCCESS; 					// Z->emu
	}
	else if(m_signal_channel == "Zee"){ 
		if( nelecs_good == 0 || nmuons_good != 0 ) return EL::StatusCode::SUCCESS; 				// Z->ee
	}
	else if(m_signal_channel == "Zmumu"){
		if( nelecs_good != 0 || nmuons_good == 0 ) return EL::StatusCode::SUCCESS; 				// Z->mumu
	}
	if(cutflow) m_hist_2lep_CutFlow_noWght->Fill(m_hist_2lep_CutFlow_noWght->GetXaxis()->FindBin("Has Lepton"));
	if(cutflow) m_hist_2lep_CutFlow->Fill(m_hist_2lep_CutFlow->GetXaxis()->FindBin("Has Lepton"), m_weight);


	if(m_signal_channel == "Zemu"){
		if( nelecs_loose > 1 || nmuons_loose > 1 ) return EL::StatusCode::SUCCESS; 				// Z->emu
	}
    else if(m_signal_channel == "Zee"){
		if( nelecs_loose > 2 || nmuons_loose != 0 ) return EL::StatusCode::SUCCESS; 			// Z->ee
	}
    else if(m_signal_channel == "Zmumu"){
		if( nelecs_loose != 0 || nmuons_loose > 2 ) return EL::StatusCode::SUCCESS; 			// Z->mumu
	}
    if(cutflow) m_hist_2lep_CutFlow_noWght->Fill(m_hist_2lep_CutFlow_noWght->GetXaxis()->FindBin("veto additional lepton"));
    if(cutflow) m_hist_2lep_CutFlow->Fill(m_hist_2lep_CutFlow->GetXaxis()->FindBin("veto additional lepton"), m_weight);


    if(m_signal_channel == "Zemu"){
		if( nelecs_good != 1 || nmuons_good != 1 ) return EL::StatusCode::SUCCESS; 				// Z->emu
	}
    else if(m_signal_channel == "Zee"){
		if( nelecs_good != 2 || nmuons_good != 0 ) return EL::StatusCode::SUCCESS; 				// Z->ee
	}
    else if(m_signal_channel == "Zmumu"){
		if( nelecs_good != 0 || nmuons_good != 2 ) return EL::StatusCode::SUCCESS; 				// Z->mumu
	}
	double leptonSF_el = 1.0;
	double leptonSF_mu = 1.0;
	double JvtSF_jet = 1.0;
	if( m_isMC ){ // SFs need to be developed for Z->ee and Z->mumu
		//std::cout<< "el_good.size: " << el_good.size() << std::endl;
		// Z->emu el mu scale factor
		if(el_good.size() == 1 && mu_good.size() == 1){
			leptonSF_el *= Props::effSFReco.get(el_good.at(0)); 
		    //leptonSF_el *= Props::effSFIsoGradientMediumLH.get(el_good.at(0)); // now we are using tightLH for el
		    leptonSF_el *= Props::effSFIsoGradientTightLH.get(el_good.at(0));
			//leptonSF_el *= Props::effSFmediumLH.get(el_good.at(0));
			leptonSF_el *= Props::effSFtightLH.get(el_good.at(0));
            leptonSF_mu *= Props::TTVAEffSF.get(mu_good.at(0));
            //leptonSF_mu *= Props::effSF.get(mu_good.at(0)); // Props::effSF.get(mu) is Props::effSF.set(muon, looseEffSF), here we use mediumEffSF
            leptonSF_mu *= Props::gradientIsoSF.get(mu_good.at(0));
            leptonSF_mu *= Props::mediumEffSF.get(mu_good.at(0));
		}
		// Z->ee leptons scale factor
        if(el_good.size() == 2 && mu_good.size() == 0){ 
            leptonSF_el *= Props::effSFReco.get(el_good.at(0));
            leptonSF_el *= Props::effSFReco.get(el_good.at(1));
            //leptonSF_el *= Props::effSFIsoGradientMediumLH.get(el_good.at(0));
            leptonSF_el *= Props::effSFIsoGradientTightLH.get(el_good.at(0));
            //leptonSF_el *= Props::effSFIsoGradientMediumLH.get(el_good.at(1));
            leptonSF_el *= Props::effSFIsoGradientTightLH.get(el_good.at(1));
            //leptonSF_el *= Props::effSFmediumLH.get(el_good.at(0));
            leptonSF_el *= Props::effSFtightLH.get(el_good.at(0));
            //leptonSF_el *= Props::effSFmediumLH.get(el_good.at(1));
            leptonSF_el *= Props::effSFtightLH.get(el_good.at(1));
        }
		// Z->mumu leptons scale factor
        if(el_good.size() == 0 && mu_good.size() == 2){
	        leptonSF_mu *= Props::TTVAEffSF.get(mu_good.at(0));
	        leptonSF_mu *= Props::TTVAEffSF.get(mu_good.at(1));
            //leptonSF_mu *= Props::effSF.get(mu_good.at(0));
            //leptonSF_mu *= Props::effSF.get(mu_good.at(1));
            leptonSF_mu *= Props::gradientIsoSF.get(mu_good.at(0));
            leptonSF_mu *= Props::gradientIsoSF.get(mu_good.at(1));
            leptonSF_mu *= Props::mediumEffSF.get(mu_good.at(0));
            leptonSF_mu *= Props::mediumEffSF.get(mu_good.at(1));
        }
		m_weight *= leptonSF_el;
	    m_weight *= leptonSF_mu;
		m_tree->leptonSF_el = leptonSF_el;	
		m_tree->leptonSF_mu = leptonSF_mu;	
		if(jet_JvtSF.size()!=0){ 
			for(unsigned int ijet=0; ijet< jet_JvtSF.size(); ijet++){
				JvtSF_jet *= jet_JvtSF[ijet];
			}	
		}
		m_weight *= JvtSF_jet;
		// because the Jvt cut is only applied to the leading jet, not all the jets, and JvtSF belongs to eventinfo container?
		//if(jet_JvtSF.size()!=0) m_weight *= jet_JvtSF.at(0); 
		m_tree->JvtSF_jet = JvtSF_jet;
	} // Scale Factors end



	if(cutflow) m_hist_2lep_CutFlow_noWght->Fill(m_hist_2lep_CutFlow_noWght->GetXaxis()->FindBin("1 el 1 mu"));
	if(cutflow) m_hist_2lep_CutFlow->Fill(m_hist_2lep_CutFlow->GetXaxis()->FindBin("1 el 1 mu"), m_weight);

	// trigger match
	// after determining one qualified el and one qualifies mu in this event, the object triggerMatch can be usedi, represent the only one.
    if(!electronTriggerMatch && !muonTriggerMatch){
        if(m_debug) Info("fill_Zemu()","No Leptons Match Trigger");
        return EL::StatusCode::SUCCESS;
    }//end if

	/////////// trigger match efficiencies and SF/////////////////////
	double triggerSF = 1.;
	passLepTrigger(triggerSF,selectionResult);
	//std::cout<< "triggerSF: " << triggerSF << "\n";
	if(m_isMC) m_weight *= triggerSF;
	m_tree->trigSF = triggerSF;



	if(cutflow) m_hist_2lep_CutFlow_noWght->Fill(m_hist_2lep_CutFlow_noWght->GetXaxis()->FindBin("least 1 lep match trigger"));
	if(cutflow) m_hist_2lep_CutFlow->Fill(m_hist_2lep_CutFlow->GetXaxis()->FindBin("least 1 lep match trigger"), m_weight);

	// opposite charge
    if(m_signal_channel == "Zemu"){
		//if( charge_el[0] * charge_mu[0] != 1 ) return StatusCode::SUCCESS; 					// used for calculating QCD background
		if( charge_el[0] * charge_mu[0] != -1 ) return StatusCode::SUCCESS; 					// Z->emu
	}
    else if(m_signal_channel == "Zee"){
		if( charge_el[0] * charge_el[1] != -1 ) return StatusCode::SUCCESS; 					// Z->ee
	}
    else if(m_signal_channel == "Zmumu"){
		if( charge_mu[0] * charge_mu[1] != -1 ) return StatusCode::SUCCESS; 					// Z->mumu
	}
	if(cutflow) m_hist_2lep_CutFlow_noWght->Fill(m_hist_2lep_CutFlow_noWght->GetXaxis()->FindBin("opposite charge"));
	if(cutflow) m_hist_2lep_CutFlow->Fill(m_hist_2lep_CutFlow->GetXaxis()->FindBin("opposite charge"), m_weight);


	// truth level check
	/*
  	if (!m_truthParts) {
    	Error("fill_Zemu()", "Did not find truth particles!");
    	return EL::StatusCode::FAILURE;
  	}
	truthCheck_TruthLevel(m_truthParts);
	*/

	//b-veto
    /////////////////////////////////////////////////////////////
    // **Calculate** : b-tagging SF
	// here the btagWeight includes the passed btag and non-passed btag jet ScaleFactor, calculated from CDI file
	// http://atlas.web.cern.ch/Atlas/GROUPS/DATABASE/GroupData/xAODBTaggingEfficiency/13TeV/
	// passed btag SF(BTaggingTool::getScaleFactor) is <1.0, while non-passed SF(BTaggingTool::getInefficiencyScaleFactor) is >1.0, 
	// but overall they are normalized to center at exactly 1, according to "closure" process, 
	// applying the ScaleFactor after btag cut, ensure any cut on b-tag will not affect the MC/data consistency
    float btagWeight = 1.; // for later use
    if (m_isMC) {
	  /*
      if ( m_physicsMeta.regime == PhysicsMetadata::Regime::resolved ) {
        btagWeight = computeBTagSFWeight(jets_btagged, m_jetReader->getContainerName());
      }
      else if ( m_physicsMeta.regime == PhysicsMetadata::Regime::merged ) {
        btagWeight = computeBTagSFWeight(trackJetsForBTagging, m_trackJetReader->getContainerName());
      }
      */
	  //btagWeight = computeBTagSFWeight(jets_btagged, m_jetReader->getContainerName());
	  btagWeight = computeBTagSFWeight(jets_good, m_jetReader->getContainerName());
      m_tree->Jets_btagSF = btagWeight;
	  //std::cout<< "btagWeight: " << btagWeight <<std::endl;
      //m_weight  *= btagWeight;
      //if(m_doTruthTagging) BTagProps::truthTagEventWeight.set(m_eventInfo,btagWeight);
    }

	if(n_bjets>=1) return StatusCode::SUCCESS;
	//if(n_bjets>=2) return StatusCode::SUCCESS;
	//if(n_bjets>=3) return StatusCode::SUCCESS;
	m_weight *= btagWeight;

	
    if(cutflow) m_hist_2lep_CutFlow_noWght->Fill(m_hist_2lep_CutFlow_noWght->GetXaxis()->FindBin("bjet-veto"));
    if(cutflow) m_hist_2lep_CutFlow->Fill(m_hist_2lep_CutFlow->GetXaxis()->FindBin("bjet-veto"), m_weight);


    // MET
    //if( met->met()/GeV >= 20 ) return StatusCode::SUCCESS;
    if(cutflow) m_hist_2lep_CutFlow_noWght->Fill(m_hist_2lep_CutFlow_noWght->GetXaxis()->FindBin("met<20GeV"));
    if(cutflow) m_hist_2lep_CutFlow->Fill(m_hist_2lep_CutFlow->GetXaxis()->FindBin("met<20GeV"), m_weight);

	// max pT Jet
	//if(jets_good.size()!=0)
    	//if(jetVec_leading.Pt()/GeV >= 30) return StatusCode::SUCCESS;
    if(cutflow) m_hist_2lep_CutFlow_noWght->Fill(m_hist_2lep_CutFlow_noWght->GetXaxis()->FindBin("pTmax Jet<30GeV"));
    if(cutflow) m_hist_2lep_CutFlow->Fill(m_hist_2lep_CutFlow->GetXaxis()->FindBin("pTmax Jet<30GeV"), m_weight);

	// reconstruct Z boson from electrons and muons
    TLorentzVector ZVec; 						// Z->emu
    if(m_signal_channel == "Zemu"){
    	ZVec = el_good.at(0)->p4() + mu_good.at(0)->p4(); 						// Z->emu
	}
    else if(m_signal_channel == "Zee"){
    	ZVec = el_good.at(0)->p4() + el_good.at(1)->p4(); 						// Z->ee
	}
    else if(m_signal_channel == "Zmumu"){
    	ZVec = mu_good.at(0)->p4() + mu_good.at(1)->p4(); 						// Z->mumu
	}

    // the lowest Z mass limit 70<m and highest m>110
    //if(ZVec.M()/GeV < 70) return StatusCode::SUCCESS;
    if(ZVec.M()/GeV < 70 || ZVec.M()/GeV > 110) return StatusCode::SUCCESS;

	// the Z mass region 85<m<95, 120<m<130
	//if(ZVec.M()/GeV < 85 || ZVec.M()/GeV > 95) return StatusCode::SUCCESS; // for Z->emu
	//if(ZVec.M()/GeV < 120 || ZVec.M()/GeV > 130) return StatusCode::SUCCESS; // for H->emu

	// the side band region m<85 || m>95 || m<120 || m>130
    //if((ZVec.M()/GeV >= 85 && ZVec.M()/GeV <= 95) || ZVec.M()/GeV <= 70 || ZVec.M()/GeV >=110) return StatusCode::SUCCESS;
    //if((ZVec.M()/GeV >= 85 && ZVec.M()/GeV <= 95) || ZVec.M()/GeV <= 70 || (ZVec.M()/GeV >= 120 && ZVec.M()/GeV <= 130) || ZVec.M()/GeV >=160) return StatusCode::SUCCESS;

	/////////////// to check the abnormal negative MC event weight /////////////////
	if(jetVec.Pt()/GeV >= 70 && jetVec.Pt()/GeV <= 80 && m_weight < -100){
		std::cout << "#" << eventInfo->eventNumber() << " : pu=" << Props::PileupweightRecalc.get(eventInfo) << "   mc= " << Props::MCEventWeight.get(eventInfo) << "   total event weight= " << m_weight << std::endl;
	}

	m_tree->Z_m = ZVec.M()/GeV;
	m_tree->Z_pT = ZVec.Pt()/GeV;
	m_tree->Z_eta = ZVec.Eta();
	m_tree->Z_phi = ZVec.Phi();
	//m_tree->Z_e = ZVec.E()/GeV;

	////////////// fill all the weighted historgrams for plotting /////////////////////////////////
	// event info
	m_hist_EventWeight->Fill(m_weight);
	if( m_isMC ) m_hist_MCEventWeight->Fill(Props::MCEventWeight.get(m_eventInfo));
	m_hist_LumiWeight->Fill(Props::LumiWeight.get(m_eventInfo));
	m_hist_PileupWeight->Fill(Props::PileupweightRecalc.get(eventInfo));
	m_hist_averageInteractionsPerCrossingRecalc_afterCuts->Fill(Props::averageInteractionsPerCrossingRecalc.get(eventInfo), Props::PileupweightRecalc.get(eventInfo));
	m_hist_leptonSF_el->Fill(leptonSF_el);
	m_hist_leptonSF_mu->Fill(leptonSF_mu);
    // electron kinematics(weighted)
    m_hist_nElectrons->Fill(el_good.size(), m_weight); 
	TLorentzVector lepVec_el_histo;
	if(el_good.size()!=0){
		for(unsigned int iEl=0; iEl<el_good.size(); iEl++){
			lepVec_el_histo = el_good.at(iEl)->p4();
	    	m_hist_electron_pT->Fill(lepVec_el_histo.Pt()/GeV, m_weight);
	    	m_hist_electron_e->Fill(lepVec_el_histo.E()/GeV, m_weight);
	    	m_hist_electron_phi->Fill(lepVec_el_histo.Phi(), m_weight);
	    	m_hist_electron_eta->Fill(lepVec_el_histo.Eta(), m_weight); 
    		m_histSvc->BookFillHist("electron_pT", 200, 0, 200, lepVec_el_histo.Pt()/GeV, m_weight);
    		m_histSvc->BookFillHist("electron_e", 300, 0, 600, lepVec_el_histo.E()/GeV, m_weight);
    		m_histSvc->BookFillHist("electron_phi", 80, -4, 4, lepVec_el_histo.Phi(), m_weight);
    		m_histSvc->BookFillHist("electron_eta", 80, -4, 4, lepVec_el_histo.Eta(), m_weight);
		}
	}
	lepVec_el_histo.Delete();
    // muon kinematics(weighted)
    m_hist_nMuons->Fill(mu_good.size(), m_weight); 
	TLorentzVector lepVec_mu_histo;
	if(mu_good.size()!=0){
		for(unsigned int iMu=0; iMu<mu_good.size(); iMu++){
			lepVec_mu_histo = mu_good.at(iMu)->p4();
	    	m_hist_muon_pT->Fill(lepVec_mu_histo.Pt()/GeV, m_weight);
	    	m_hist_muon_e->Fill(lepVec_mu_histo.E()/GeV, m_weight);
	    	m_hist_muon_phi->Fill(lepVec_mu_histo.Phi(), m_weight);
	    	m_hist_muon_eta->Fill(lepVec_mu_histo.Eta(), m_weight);
            m_histSvc->BookFillHist("muon_pT", 200, 0, 200, lepVec_mu_histo.Pt()/GeV, m_weight); 
            m_histSvc->BookFillHist("muon_e", 300, 0, 600, lepVec_mu_histo.E()/GeV, m_weight); 
            m_histSvc->BookFillHist("muon_phi", 80, -4, 4, lepVec_mu_histo.Phi(), m_weight); 
            m_histSvc->BookFillHist("muon_eta", 80, -4, 4, lepVec_mu_histo.Eta(), m_weight); 
		}
	}
	lepVec_mu_histo.Delete();
    // jet kinematics(weighted)	
    m_hist_nJets->Fill(jets_good.size(), m_weight); 
	TLorentzVector lepVec_jet_histo;
	if(jets_good.size()!=0){
		for(unsigned int iJet=0; iJet<jets_good.size(); iJet++){
			lepVec_jet_histo = jets_good.at(iJet)->p4();
	    	m_hist_jet_pT->Fill(lepVec_jet_histo.Pt()/GeV, m_weight);
	    	m_hist_jet_e->Fill(lepVec_jet_histo.E()/GeV, m_weight);
	    	m_hist_jet_phi->Fill(lepVec_jet_histo.Phi(), m_weight);
	    	m_hist_jet_eta->Fill(lepVec_jet_histo.Eta(), m_weight);
            m_histSvc->BookFillHist("jet_pT", 200, 0, 200, lepVec_jet_histo.Pt()/GeV, m_weight);
            m_histSvc->BookFillHist("jet_e", 200, 0, 200, lepVec_jet_histo.E()/GeV, m_weight);
            m_histSvc->BookFillHist("jet_phi", 80, -4, 4, lepVec_jet_histo.Phi(), m_weight);
            m_histSvc->BookFillHist("jet_eta", 80, -4, 4, lepVec_jet_histo.Eta(), m_weight); 
		}
        m_hist_leading_jet_pT->Fill((jets_good.at(0)->p4()).Pt()/GeV, m_weight);
        m_histSvc->BookFillHist("leading_jet_pT", 200, 0, 200, (jets_good.at(0)->p4()).Pt()/GeV, m_weight);
        m_histSvc->BookFillHist("jet_nbjet", 5, 0, 5, n_bjets , m_weight);
        //m_histSvc->BookFillHist("jet_btagSF", 300, 0, 3, btagWeight);
	}
    if(jets_good.size()==0){
            m_hist_jet_pT->Fill(0., m_weight);
            m_hist_leading_jet_pT->Fill(0., m_weight);
            m_hist_jet_e->Fill(0., m_weight);
            m_histSvc->BookFillHist("jet_pT", 200, 0, 200, 0., m_weight);
            m_histSvc->BookFillHist("leading_jet_pT", 200, 0, 200, 0., m_weight);
            m_histSvc->BookFillHist("jet_e", 200, 0, 200, 0., m_weight);
        	m_histSvc->BookFillHist("jet_nbjet", 5, 0, 5, n_bjets , m_weight);
    } // if no jet, still need to fill the histo in the pT and e as 0
	lepVec_jet_histo.Delete();
	// MET
	m_hist_MET_met->Fill(met->met()/GeV, m_weight);
    m_histSvc->BookFillHist("MET", 200, 0, 200, met->met()/GeV, m_weight);

	// Z(ee, mumu, emu) boson (weighted)
    m_hist_Z_pT_weighted->Fill(ZVec.Pt()/GeV, m_weight);
    m_hist_Z_eta_weighted->Fill(ZVec.Eta(), m_weight);
    m_hist_Z_phi_weighted->Fill(ZVec.Phi(), m_weight);
    m_hist_Z_e_weighted->Fill(ZVec.E()/GeV, m_weight);
	m_hist_Z_mass_weighted->Fill(ZVec.M()/GeV, m_weight);

	//////// the loop of optimization of the MET cuts and jet pT cuts ////////////////
	/*
	// for the signal Zemu MC
	for(int i_met=0;i_met<100;i_met++){

		for(int i_JetPt=0;i_JetPt<100;i_JetPt++){

			if(met->met()/GeV>=i_met) continue;
			if(jets_good.size()!=0)
				if(jetVec.Pt()/GeV >= i_JetPt) continue;
			m_hist_cut_optimization->Fill(i_met, i_JetPt, m_weight);
		
		}
	}

	//for the data driven background, obtained from fitting integral
	*/


	/////////////////////////////////////////////

	m_histSvc->BookFillHist("Z_pT", 300, 0, 300, ZVec.Pt()/GeV, m_weight);
	m_histSvc->BookFillHist("Z_eta", 200, -10, 10, ZVec.Eta(), m_weight);
	m_histSvc->BookFillHist("Z_phi", 80, -4, 4, ZVec.Phi(), m_weight);
	m_histSvc->BookFillHist("Z_e", 400, 0, 800, ZVec.E()/GeV, m_weight);
	m_histSvc->BookFillHist("Z_Mass", 600, 0, 300, ZVec.M()/GeV, m_weight);


	jetVec_leading.Delete();
	jetVec_subleading.Delete();
    ZVec.Delete();

	/*
    m_tree->Leptons_eventTriggers.push_back(m_passHLT_e24_lhmedium_L1EM20VH);
        m_tree->Leptons_eventTriggers.push_back(m_passHLT_e60_lhmedium);
        m_tree->Leptons_eventTriggers.push_back(m_passHLT_mu20_iloose_L1MU15);
        m_tree->Leptons_eventTriggers.push_back(m_passHLT_mu50);
    //    m_tree->Leptons_eventTriggers.push_back(m_passHLT_e24_lhvloose_L1EM20VH);

    TLorentzVector lepVec;
    int electronTriggerMatch = 0;
    if( m_debug ) Info("fill_Zemu()", "Checking Electrons");
    for(unsigned int i = 0; i < electron.size(); i++){
        //if( !passesLooseCuts(electron[i]) ) continue; //Skip this electron
        lepVec=electron[i]->p4();

        m_tree->Leptons_electron_pT.push_back(lepVec.Pt()/GeV);
        m_tree->Leptons_electron_eta.push_back(lepVec.Eta());
        m_tree->Leptons_electron_phi.push_back(lepVec.Phi());
        m_tree->Leptons_electron_e.push_back(lepVec.E()/GeV);

        m_tree->Leptons_electron_charge.push_back(electron[i]->charge());
        std::vector<bool> passTriggers;
            passTriggers.push_back(m_passHLT_e24_lhmedium_L1EM20VH);
            passTriggers.push_back(m_passHLT_e60_lhmedium);
            passTriggers.push_back(m_passHLT_mu20_iloose_L1MU15);
            passTriggers.push_back(m_passHLT_mu50);
            //passTriggers.push_back(m_passHLT_e24_lhvloose_L1EM20VH);
        //electronTriggerMatch |= m_passHLT_e24_lhvloose_L1EM20VH || m_passHLT_e24_lhmedium_L1EM20VH || m_passHLT_e60_lhmedium;
		electronTriggerMatch = 1;

        m_tree->Leptons_electron_passTriggers.push_back(passTriggers);
        //m_tree->Leptons_electron_pTcone20.push_back(LLPProps::ptvarcone20.get(electron[i])/GeV);
        m_tree->Leptons_electron_d0.push_back(Props::d0.get(electron[i]));
        m_tree->Leptons_electron_d0sig.push_back(Props::d0sig.get(electron[i]));
        m_tree->Leptons_electron_z0.push_back(Props::z0.get(electron[i]));
        m_tree->Leptons_electron_isGradientIso.push_back(Props::isGradientIso.get(electron[i]));
        if( m_isMC ){
            m_tree->Leptons_electron_scaleFactor.push_back(leptonSF*Props::effSFlooseLH.get(electron[i])*Props::effSFReco.get(electron[i])); ///TODO : Scale Factor
        }else{
            m_tree->Leptons_electron_scaleFactor.push_back(1.0);
        }//end if

        //m_tree->Leptons_electron_iso.push_back(LLPProps::ptvarcone20.get(electron[i])/lepVec.Pt());
        int quality = 0;
			//printf("Props::isLooseLH.get(electron[i]): %i \n", Props::isLooseLH.get(electron[i]));
            if( Props::isLooseLH.get(electron[i]) ) quality = 1;
            if( Props::isMediumLH.get(electron[i]) ) quality = 2;
            if( Props::isTightLH.get(electron[i]) ) quality = 3;
        m_tree->Leptons_electron_quality.push_back(quality);

        m_tree->Leptons_nElectrons += 1;
    }//end for : Loop over all electrons

    int muonTriggerMatch = 0;
    if( m_debug ) Info("fill_Zemu()", "Checking Muons.");
    for(unsigned int i = 0; i < muon.size(); i++){
        //if( !passesLooseCuts(muon[i]) ) continue; //Skip this muon
        lepVec=muon[i]->p4();

        m_tree->Leptons_muon_pT.push_back(lepVec.Pt()/GeV);
        m_tree->Leptons_muon_eta.push_back(lepVec.Eta());
        m_tree->Leptons_muon_phi.push_back(lepVec.Phi());
        m_tree->Leptons_muon_e.push_back(lepVec.E()/GeV);

        m_tree->Leptons_muon_charge.push_back(muon[i]->charge());
        std::vector<bool> passTriggers;
            passTriggers.push_back(m_passHLT_e24_lhmedium_L1EM20VH);
            passTriggers.push_back(m_passHLT_e60_lhmedium);
            passTriggers.push_back(m_passHLT_mu20_iloose_L1MU15);
            passTriggers.push_back(m_passHLT_mu50);
            //passTriggers.push_back(m_passHLT_e24_lhvloose_L1EM20VH);
        muonTriggerMatch |= m_passHLT_mu20_iloose_L1MU15 || m_passHLT_mu50;

        m_tree->Leptons_muon_passTriggers.push_back(passTriggers);
        //m_tree->Leptons_muon_pTcone20.push_back(LLPProps::ptvarcone20.get(muon[i])/GeV);
        m_tree->Leptons_muon_d0.push_back(Props::d0.get(muon[i]));
        m_tree->Leptons_muon_d0sig.push_back(Props::d0sig.get(muon[i]));
        m_tree->Leptons_muon_z0.push_back(Props::z0.get(muon[i]));
        m_tree->Leptons_muon_isGradientIso.push_back(Props::isGradientIso.get(muon[i]));
        if( m_isMC ){
            m_tree->Leptons_muon_scaleFactor.push_back(leptonSF*Props::effSF.get(muon[i])*Props::TTVAEffSF.get(muon[i])); ///TODO : Scale Factor
        }else{
            m_tree->Leptons_muon_scaleFactor.push_back(1.0);
        }//end if-else

        //m_tree->Leptons_muon_iso.push_back(LLPProps::ptvarcone20.get(muon[i])/lepVec.Pt());
        int quality = 0;
			//printf("Props::isLooseLH.get(muon[i]): %i \n", Props::isLooseLH.get(muon[i]));
            //if( Props::isLooseLH.get(muon[i]) ) quality = 1;
            //if( Props::isMediumLH.get(muon[i]) ) quality = 2;
            //if( Props::isTightLH.get(muon[i]) ) quality = 3;
        m_tree->Leptons_muon_quality.push_back(quality);

        m_tree->Leptons_nMuons += 1;
    }//end for : Loop over all muons

    if( (m_tree->Leptons_nMuons+m_tree->Leptons_nElectrons) < 1 ){
        if(m_debug) Info("fill_Zemu()","No Good Leptons In Event");
        return EL::StatusCode::SUCCESS;
    }//end if
    if(cutflow) {
        m_hist_1lep_CutFlow[0]->Fill(m_hist_1lep_CutFlow[0]->GetXaxis()->FindBin("Has Lepton"),m_weight);
        m_hist_1lep_CutFlow_noWght[0]->Fill(m_hist_1lep_CutFlow_noWght[0]->GetXaxis()->FindBin("Has Lepton"));
        m_hist_1lep_CutFlow[1]->Fill(m_hist_1lep_CutFlow[1]->GetXaxis()->FindBin("Has Lepton"),m_weight);
        m_hist_1lep_CutFlow_noWght[1]->Fill(m_hist_1lep_CutFlow_noWght[1]->GetXaxis()->FindBin("Has Lepton"));
        m_hist_1lep_CutFlow[2]->Fill(m_hist_1lep_CutFlow[2]->GetXaxis()->FindBin("Has Lepton"),m_weight);
        m_hist_1lep_CutFlow_noWght[2]->Fill(m_hist_1lep_CutFlow_noWght[2]->GetXaxis()->FindBin("Has Lepton"));
    }//end if - tell each cutflow the total number of events [entry 0]
	*/

    /** Check Trigger Match **/
	/*
    if(!electronTriggerMatch && !muonTriggerMatch){
        if(m_debug) Info("fill_Zemu()","No Leptons Match Trigger");
        return EL::StatusCode::SUCCESS;
    }//end if
    if(cutflow) {
        m_hist_1lep_CutFlow[0]->Fill(m_hist_1lep_CutFlow[0]->GetXaxis()->FindBin("Trig Match"),m_weight);
        m_hist_1lep_CutFlow_noWght[0]->Fill(m_hist_1lep_CutFlow_noWght[0]->GetXaxis()->FindBin("Trig Match"));
        m_hist_1lep_CutFlow[1]->Fill(m_hist_1lep_CutFlow[1]->GetXaxis()->FindBin("Trig Match"),m_weight);
        m_hist_1lep_CutFlow_noWght[1]->Fill(m_hist_1lep_CutFlow_noWght[1]->GetXaxis()->FindBin("Trig Match"));
        m_hist_1lep_CutFlow[2]->Fill(m_hist_1lep_CutFlow[2]->GetXaxis()->FindBin("Trig Match"),m_weight);
        m_hist_1lep_CutFlow_noWght[2]->Fill(m_hist_1lep_CutFlow_noWght[2]->GetXaxis()->FindBin("Trig Match"));
    }//end if - tell each cutflow the total number of events [entry 0]
	*/

    /** MET **/
/*
    //Add the MET to the event
    if( !met_cst ){
        std::cerr << "MET POINTER NOT INITIALIZED!" << std::endl;
        return EL::StatusCode::SUCCESS;
    }//end if
    m_tree->MET_CST_met = met_cst->met()/GeV;
    m_tree->MET_CST_phi = met_tst->phi();
    m_tree->MET_TST_met = met_tst->met()/GeV;
    m_tree->MET_TST_phi = met_tst->phi();
    m_tree->CST_TST_Diff = met_cst->met()/GeV - met_tst->met()/GeV;
    m_tree->CST_TST_Diff_phi = met_cst->phi() - met_tst->phi();
*/

    /** Check Jets **/
	/*
    //bool isBTagged = false;
    TLorentzVector jetVec;
    if(m_debug) Info("fill_Zemu()", "Begin loop over jets");
    for(unsigned int iJet=0; iJet < jets.size(); iJet++){
        jetVec = jets[iJet]->p4();

        //Only keep this jet if it passes basic kinematic criteria
        if( jetVec.Et() < 20*GeV || fabs(jetVec.Eta()) > 2.5 ) continue;
        //Write the jet information to the histograms.
        m_tree->Jets_E.push_back(jetVec.E()/GeV);
        m_tree->Jets_pT.push_back(jetVec.Pt()/GeV);
        m_tree->Jets_eta.push_back(jetVec.Eta());
        m_tree->Jets_phi.push_back(jetVec.Phi());

        //m_tree->Jets_isClean.push_back(LLPProps::isCleanJet.get(jets[iJet]));
        //m_tree->Jets_timing.push_back(LLPProps::Timing.get(jets[iJet]));
        m_tree->Jets_nTrks.push_back(Props::NumTrkPt1000PV.get(jets[iJet]));
        ///m_tree->Jets_isBTagged.push_back(BTagProps::isTagged.get(jets[iJet]));


        //m_tree->Jets_chFrac.push_back(LLPProps::EMFrac.get(jets[iJet]));
        //m_tree->Jets_logRatio.push_back(getLogRatio(LLPProps::EMFrac.get(jets[iJet])));
        //bool isCr = isCRjet(jets[iJet]);
        //m_tree->Jets_isCR.push_back( isCr );
        //m_tree->Jets_width.push_back(LLPProps::Width.get(jets[iJet]));

        m_tree->Jets_mv2c10.push_back(Props::MV2c10.get(jets[iJet]));
        m_tree->Jets_mv2c20.push_back(Props::MV2c20.get(jets[iJet]));

        //if( isCr ) m_tree->Jets_nCRJets += 1;
        m_tree->Jets_nJets += 1;
    }//end for : loop over jets

    if(cutflow) {
        m_hist_1lep_CutFlow[0]->Fill(m_hist_1lep_CutFlow[0]->GetXaxis()->FindBin("CRJet"),m_weight);
        m_hist_1lep_CutFlow_noWght[0]->Fill(m_hist_1lep_CutFlow_noWght[0]->GetXaxis()->FindBin("CRJet"));
        m_hist_1lep_CutFlow[1]->Fill(m_hist_1lep_CutFlow[1]->GetXaxis()->FindBin("CRJet"),m_weight);
        m_hist_1lep_CutFlow_noWght[1]->Fill(m_hist_1lep_CutFlow_noWght[1]->GetXaxis()->FindBin("CRJet"));
        m_hist_1lep_CutFlow[2]->Fill(m_hist_1lep_CutFlow[2]->GetXaxis()->FindBin("CRJet"),m_weight);
        m_hist_1lep_CutFlow_noWght[2]->Fill(m_hist_1lep_CutFlow_noWght[2]->GetXaxis()->FindBin("CRJet"));
    }///end if : Bad Jet Veto Bin (TODO : can be removed)
	*/

    /** Check Truth **/
	
    //if(m_debug) Info("fill_Zemu()", "Checking Truth Information");
    //if( m_isMC /** TODO **/ ){
    /*    for(unsigned int i = 0; i < truthParticles.size(); i++){
            const xAOD::TruthParticle* particle = truthParticles.at(i);

            if( LLPProps::isH.get(particle) ){
                //Store the kinematics
                m_tree->TruthStorage_H_e.push_back(particle->e()/GeV);
                m_tree->TruthStorage_H_pT.push_back(particle->pt()/GeV);
                m_tree->TruthStorage_H_eta.push_back(particle->eta());
                m_tree->TruthStorage_H_phi.push_back(particle->phi());
            }else if( LLPProps::isLLP.get(particle) ){
                //This particle is an LLP
                m_tree->TruthStorage_LLP_e.push_back( particle->e()/GeV );
                m_tree->TruthStorage_LLP_pT.push_back( particle->pt()/GeV );
                m_tree->TruthStorage_LLP_phi.push_back( particle->phi() );
                m_tree->TruthStorage_LLP_eta.push_back( particle->eta() );

                m_tree->TruthStorage_LLP_Lxy.push_back(LLPProps::Lxy.get(particle));
                m_tree->TruthStorage_LLP_Lz.push_back(LLPProps::Lz.get(particle));
                m_tree->TruthStorage_LLP_decayLength.push_back(LLPProps::decayLength.get(particle));
                m_tree->TruthStorage_LLP_decayTime.push_back(LLPProps::decayTime.get(particle));
                m_tree->TruthStorage_LLP_flightTime.push_back(LLPProps::flightTime.get(particle));
                m_tree->TruthStorage_LLP_lifetime.push_back( LLPProps::properLifetime.get(particle) );
            }else if( LLPProps::isZ.get(particle) && particle->pt() > 0 ){
                m_tree->TruthStorage_Z_e.push_back(particle->e()/GeV);
                m_tree->TruthStorage_Z_pT.push_back(particle->pt()/GeV);
                m_tree->TruthStorage_Z_eta.push_back(particle->eta());
                m_tree->TruthStorage_Z_phi.push_back(particle->phi());
                std::cout << " Z Status : " << particle->status() << std::endl;
            }else if( LLPProps::isW.get(particle) && particle->pt() > 0 ){
                m_tree->TruthStorage_W_e.push_back(particle->e()/GeV);
                m_tree->TruthStorage_W_pT.push_back(particle->pt()/GeV);
                m_tree->TruthStorage_W_eta.push_back(particle->eta());
                m_tree->TruthStorage_W_phi.push_back(particle->phi());
            }//end else-if
        }//end for : Loop over all truth Particles

        if(cutflow) {
            m_hist_1lep_CutFlow[0]->Fill(m_hist_1lep_CutFlow[0]->GetXaxis()->FindBin("TruthInfo"),m_weight);
            m_hist_1lep_CutFlow_noWght[0]->Fill(m_hist_1lep_CutFlow_noWght[0]->GetXaxis()->FindBin("TruthInfo"));
            m_hist_1lep_CutFlow[1]->Fill(m_hist_1lep_CutFlow[1]->GetXaxis()->FindBin("TruthInfo"),m_weight);
            m_hist_1lep_CutFlow_noWght[1]->Fill(m_hist_1lep_CutFlow_noWght[1]->GetXaxis()->FindBin("TruthInfo"));
            m_hist_1lep_CutFlow[2]->Fill(m_hist_1lep_CutFlow[2]->GetXaxis()->FindBin("TruthInfo"),m_weight);
            m_hist_1lep_CutFlow_noWght[2]->Fill(m_hist_1lep_CutFlow_noWght[2]->GetXaxis()->FindBin("TruthInfo"));
        }///end if : TODO : Add the TruthInfo to cutflow
    }//end if : get truth information
	*/

    //Apply the MC weight from the sum of weights file
	//m_weight is already defined in AnalysisReader::setEventWeight
	//MCEventWeight, LumiWeight, PUWeight are already included
    //if(m_isMC) m_weight *= Props::MCEventWeight.get(m_eventInfo); 
	
    if(m_isMC){
        m_tree->MCEventWeight = Props::MCEventWeight.get(m_eventInfo);
        m_tree->LumiWeight = Props::LumiWeight.get(m_eventInfo); //Grab the lumi weight
    }else{
        m_tree->MCEventWeight = 1;
        m_tree->LumiWeight = 1;
    }//end if-else	
	
    m_tree->EventWeight = m_weight;
    m_tree->PileupWeight = Props::PileupweightRecalc.get(eventInfo);

	//std::cout << "MCEventWeight : " << Props::MCEventWeight.get(m_eventInfo) << std::endl;
	//std::cout << "LumiWeight : " << Props::LumiWeight.get(m_eventInfo) << std::endl;
	//std::cout << "PileupWeight : " << Props::PileupweightRecalc.get(eventInfo) << std::endl;
    //std::cout << "EventWeight : " << m_weight << std::endl << std::endl;

    //m_tree->mu = Props::averageInteractionsPerCrossingRecalc.get(eventInfo);
    //m_tree->mu_origin = Props::averageInteractionsPerCrossing.get(m_eventInfo);
    //m_tree->mu_weighted = Props::averageInteractionsPerCrossingRecalc.get(eventInfo)*Props::PileupweightRecalc.get(eventInfo);

    //Copy the event weight over to the tree

    // event weight scale factors are not applied for the moment
    if(m_isMC) m_tree->mcChannelNumber = eventInfo->mcChannelNumber(); //datasetid

	m_tree->SetVariation(m_currentVar);

	//std::cout << "inside AnalysisReader_Zemu :: fill_Zemu(), m_currentVar:   " << m_currentVar  << "\n" << std::endl;
	
    if(m_debug) Info("fill_Zemu()", "Filling TTree");
    // here fill the tree doesn't mean that the leaves info are after all the cuts 
	// m_tree->Fill() only decides whether we save this event 
	// but when the event passes all the cuts, the leaves really depends on where I fill the leaves rather than where to fill the tree
    m_tree->Fill();  

    if(m_debug) Info("fill_Zemu()", "Done in fill_Zemu()");
    return EL::StatusCode::SUCCESS;
}//end fill_Zemu() function


bool AnalysisReader_Zemu::passLepTrigger(double& triggerSF_nominal, ZemuResult selectionResult){
  std::vector<const xAOD::Electron*>  el;
  std::vector<const xAOD::Muon*>      mu;
  el.clear();
  mu.clear();
  if(selectionResult.el.size() > 0) el = selectionResult.el;
  if(selectionResult.mu.size() > 0) mu = selectionResult.mu;  
  //std::vector<const xAOD::Muon*>      mu                = selectionResult.mu;

  //bool isMu = (mu1&&mu2);
  //bool isE = (el1&&el2);

  CP::SystematicSet sysSet;
  m_triggerTool -> applySystematicVariation(sysSet);
  m_triggerTool -> setEventInfo(m_eventInfo, m_randomRunNumber);
  m_triggerTool -> setElectrons(el);
  //m_triggerTool -> setElectrons({el1, el2});
  m_triggerTool -> setMuons(mu);
  //m_triggerTool -> setMuons({mu1, mu2});

  bool triggerDec = m_triggerTool -> getDecisionAndScaleFactor(triggerSF_nominal);

  if (!triggerDec) return false;

  // handle systematics
  
  if (m_isMC && (m_currentVar == "Nominal")) {
    for (size_t i = 0; i < m_triggerSystList.size(); i++) {
      // not computing useless systematics
      if (el.size() > 0  && (m_triggerSystList.at(i).find("MUON_EFF_Trig") != std::string::npos)) {
        m_weightSysts.push_back({ m_triggerSystList.at(i), 1.0 });
        continue;
      }

      if (mu.size() > 0 && (m_triggerSystList.at(i).find("EL_EFF_Trig") != std::string::npos)) {
        m_weightSysts.push_back({ m_triggerSystList.at(i), 1.0 });
        continue;
      }

      // get decision + weight
      double triggerSF = 1.;

      CP::SystematicSet sysSet(m_triggerSystList.at(i));
      m_triggerTool -> applySystematicVariation(sysSet);
      m_triggerTool -> getDecisionAndScaleFactor(triggerSF);

      if (triggerSF_nominal > 0) m_weightSysts.push_back({ m_triggerSystList.at(i), (float)(triggerSF / triggerSF_nominal) });
      else Error("passLepTrigger()", "Nominal trigger SF=0!, The systematics will not be generated.");
    }
  }
  

  return true;
}//passLepTrigger

void AnalysisReader_Zemu::compute_btagging ()
{
  if (m_trackJets) {
    auto previous_ja = m_bTagTool->getJetAuthor();
    auto previous_ts = m_bTagTool->getTaggingScheme();

    m_bTagTool->setJetAuthor(m_trackJetReader->getContainerName());
    m_bTagTool->setTaggingScheme("FixedCut");

    for (auto jet : *m_trackJets) {
      BTagProps::isTagged.set(jet, static_cast<decltype(BTagProps::isTagged.get(jet))>(m_bTagTool->isTagged(*jet)));
      BTagProps::tagWeight.set(jet, Props::MV2c10.get(jet));
    }

    m_bTagTool->setJetAuthor(previous_ja);
    m_bTagTool->setTaggingScheme(previous_ts);
  }

  if (m_jets) {
    auto previous_ja = m_bTagTool->getJetAuthor();
    if(!m_use2DbTagCut) m_bTagTool->setJetAuthor(m_jetReader->getContainerName());
	
	//std::cout<<"hello m_jets"<<std::endl;

    for (auto jet : *m_jets) {
      if(!m_use2DbTagCut) BTagProps::isTagged.set(jet, static_cast<decltype(BTagProps::isTagged.get(jet))>(m_bTagTool->isTagged(*jet)));
      else BTagProps::isTagged.set(jet, false);

	  //std::cout<<"BTagProps::isTagged: " << BTagProps::isTagged.get(jet) <<std::endl;

      BTagProps::tagWeight.set(jet, Props::MV2c10.get(jet));
      BTagProps::tagWeight.set(jet, Props::MV2c20.get(jet));

      if (m_isMC && !m_use2DbTagCut) BTagProps::eff.set(jet, m_bTagTool->getEfficiency(*jet));
      else BTagProps::eff.set(jet, -999.);

    }
    m_bTagTool->setJetAuthor(previous_ja);
  }

  if (m_fatJets) {
    for (auto fatjet : *m_fatJets) {
      Props::nTrackJets.set(fatjet, 0);
      Props::nBTags.set(fatjet, 0);
      if (m_isMC) Props::nTrueBJets.set(fatjet, 0);
      else Props::nTrueBJets.set(fatjet, -1);
    }
  }

} // compute_btagging




// truthCheck_TruthLevel is before the MET and the Jet_pt cuts, to plot the emu mass spectrum
bool AnalysisReader_Zemu::truthCheck_TruthLevel(const xAOD::TruthParticleContainer* truthp)
{ /* this does some basic truth analysis */

    vector<TLorentzVector> TLeptons;  TLeptons.clear();
    vector<int> Tcharge;  Tcharge.clear();
    xAOD::TruthParticleContainer::const_iterator itr;
    int n_el=0;
	int n_mu=0;
    //bool matched1=false;
    //bool matched2=false;
    for (itr = truthp->begin(); itr!=truthp->end(); ++itr)
    {
        int pdgId=(*itr)->pdgId();
		std::cout<< (*itr)->status() << std::endl;
        //TLorentzVector newp =  (*itr)->p4();
		/*
        if ( abs(pdgId) == 11 )
        {
            //cout << newp.DeltaR(e1) << endl;
            if ( !matched1 && newp.DeltaR(e1) < 0.15 )
            {
                matched1=true;
                leps*=pdgId;
            }
            if ( !matched2 && newp.DeltaR(e2) < 0.15 )
            {
                matched2=true;
                leps*=pdgId;
            }
        }*/
    }
    return true;
}



EL::StatusCode AnalysisReader_Zemu :: finalize (){
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.


  // call base to print number of events
  AnalysisReader::finalize();

  wk()->addOutput(m_hist_normalisation);


  return EL::StatusCode::SUCCESS;
}












