#include "CxAODReader_Zemu/MVATree_Zemu.h"

MVATree_Zemu::MVATree_Zemu(bool persistent, bool readMVA, std::string analysisType, EL::Worker *wk, std::vector<std::string> variations, bool nominalOnly) :
        MVATree(persistent, readMVA, wk, variations, nominalOnly),
        m_analysisType(analysisType){

  if(nominalOnly){variations = {"Nominal"};};
 
  // here we need the loop
  // because in the loop the m_currentVar can be set to be each variation, then SetBranches
  // because this is initilization of the tree, we need the variations loop to create each branch by using AddBranch().
  for (std::string varName : variations) {
    SetVariation(varName);
    SetBranches();
  }
  
  //std::cout<< "this is inside MVATree_Zemu::MVATree_Zemu, m_currentVar:" << m_currentVar << "\n\n\n" << std::endl;  
  //SetVariation(m_currentVar);
  //SetBranches();
}

void MVATree_Zemu::AddBranch(TString name, Int_t* address, bool forReader){
  m_treeMap[m_currentVar] -> Branch(name, address);
  //if (forReader) {
  //  m_reader.AddVariable(name, address);
  //}
}

void MVATree_Zemu::AddBranch(TString name, Float_t* address, bool forReader){
  m_treeMap[m_currentVar] -> Branch(name, address);
  //if (forReader) {
  //  m_reader.AddVariable(name, address);
  //}
}

void MVATree_Zemu::AddBranch(TString name, Bool_t* address, bool forReader){
  m_treeMap[m_currentVar] -> Branch(name, address);
  //if (forReader) {
  //  m_reader.AddVariable(name, address);
  //}
}

void MVATree_Zemu::SetBranches(){

    AddBranch("EventWeight", &EventWeight, false);
    AddBranch("MCEventWeight", &MCEventWeight, false);
    AddBranch("LumiWeight", &LumiWeight, false);
    AddBranch("PileupWeight", &PileupWeight, false);
    AddBranch("leptonSF_el", &leptonSF_el, false);
    AddBranch("leptonSF_mu", &leptonSF_mu, false);
    AddBranch("JvtSF_jet", &JvtSF_jet, false);
    AddBranch("UnPrescale", &UnPrescale, false);
    AddBranch("EventNumber", &EventNumber, false);
    AddBranch("mcChannelNumber", &mcChannelNumber, false);
    AddBranch("mu", &mu, false);
    AddBranch("mu_origin", &mu_origin, false);
    AddBranch("mu_weighted", &mu_weighted, false);
    AddBranch("isBlinded", &isBlinded, false);

    AddBranch("trigSF", &trigSF, false);


    ///Leptons
    m_treeMap[m_currentVar]->Branch("Leptons_eventTriggers", &Leptons_eventTriggers);

    m_treeMap[m_currentVar]->Branch("Leptons_nElectrons", &Leptons_nElectrons);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_pT", &Leptons_electron_pT);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_e", &Leptons_electron_e);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_phi", &Leptons_electron_phi);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_eta", &Leptons_electron_eta);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_pTcone20", &Leptons_electron_pTcone20);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_d0", &Leptons_electron_d0);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_d0sig", &Leptons_electron_d0sig);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_z0", &Leptons_electron_z0);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_charge", &Leptons_electron_charge);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_isGradientIso", &Leptons_electron_isGradientIso);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_passTriggers", &Leptons_electron_passTriggers);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_scaleFactor", &Leptons_electron_scaleFactor);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_iso", &Leptons_electron_iso);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_quality", &Leptons_electron_quality);

    m_treeMap[m_currentVar]->Branch("Leptons_electron_effSFReco", &Leptons_electron_effSFReco);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_effSFIsoGradientMediumLH", &Leptons_electron_effSFIsoGradientMediumLH);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_effSFmediumLH", &Leptons_electron_effSFmediumLH);
    m_treeMap[m_currentVar]->Branch("Leptons_electron_trigSFmediumLHIsoGradient", &Leptons_electron_trigSFmediumLHIsoGradient);

    m_treeMap[m_currentVar]->Branch("Leptons_nMuons", &Leptons_nMuons);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_pT", &Leptons_muon_pT);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_e", &Leptons_muon_e);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_phi", &Leptons_muon_phi);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_eta", &Leptons_muon_eta);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_pTcone20", &Leptons_muon_pTcone20);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_d0", &Leptons_muon_d0);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_d0sig", &Leptons_muon_d0sig);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_z0", &Leptons_muon_z0);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_charge", &Leptons_muon_charge);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_isGradientIso", &Leptons_muon_isGradientIso);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_passTriggers", &Leptons_muon_passTriggers);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_scaleFactor", &Leptons_muon_scaleFactor);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_iso", &Leptons_muon_iso);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_quality", &Leptons_muon_quality);

    m_treeMap[m_currentVar]->Branch("Leptons_muon_TTVAEffSF", &Leptons_muon_TTVAEffSF);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_effSF", &Leptons_muon_effSF);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_gradientIsoSF", &Leptons_muon_gradientIsoSF);
    m_treeMap[m_currentVar]->Branch("Leptons_muon_mediumEffSF", &Leptons_muon_mediumEffSF);

    ///MET
	/*
    m_treeMap[m_currentVar]->Branch("MET_CST_met", &MET_CST_met);
    m_treeMap[m_currentVar]->Branch("MET_CST_phi", &MET_CST_phi);
    m_treeMap[m_currentVar]->Branch("MET_TST_met", &MET_TST_met);
    m_treeMap[m_currentVar]->Branch("MET_TST_phi", &MET_TST_phi);
    m_treeMap[m_currentVar]->Branch("CST_TST_Diff", &CST_TST_Diff);
    m_treeMap[m_currentVar]->Branch("CST_TST_Diff_phi", &CST_TST_Diff_phi);
	*/
    m_treeMap[m_currentVar]->Branch("MET_met", &MET_met);
    m_treeMap[m_currentVar]->Branch("MET_phi", &MET_phi);

    ///JETS
    m_treeMap[m_currentVar]->Branch("Jets_nJets", &Jets_nJets);
    m_treeMap[m_currentVar]->Branch("Jets_nCRJets", &Jets_nCRJets);
    m_treeMap[m_currentVar]->Branch("Jets_leading_pT", &Jets_leading_pT);
    m_treeMap[m_currentVar]->Branch("Jets_subleading_pT", &Jets_subleading_pT);
    m_treeMap[m_currentVar]->Branch("Jets_pT", &Jets_pT);
    m_treeMap[m_currentVar]->Branch("Jets_E", &Jets_E);
    m_treeMap[m_currentVar]->Branch("Jets_eta", &Jets_eta);
    m_treeMap[m_currentVar]->Branch("Jets_phi", &Jets_phi);
    m_treeMap[m_currentVar]->Branch("Jets_Jvt", &Jets_Jvt);
    m_treeMap[m_currentVar]->Branch("Jets_nTrks", &Jets_nTrks);
    m_treeMap[m_currentVar]->Branch("Jets_logRatio", &Jets_logRatio);
    m_treeMap[m_currentVar]->Branch("Jets_chFrac", &Jets_chFrac);
    m_treeMap[m_currentVar]->Branch("Jets_isCR", &Jets_isCR);
    m_treeMap[m_currentVar]->Branch("Jets_isClean", &Jets_isClean);
    m_treeMap[m_currentVar]->Branch("Jets_timing", &Jets_timing);
    m_treeMap[m_currentVar]->Branch("Jets_width", &Jets_width);
    m_treeMap[m_currentVar]->Branch("Jets_JvtSF", &Jets_JvtSF);

    m_treeMap[m_currentVar]->Branch("Jets_isBTagged", &Jets_isBTagged);
    m_treeMap[m_currentVar]->Branch("Jets_nbjet", &Jets_nbjet);
    m_treeMap[m_currentVar]->Branch("Jets_btagSF", &Jets_btagSF);
    m_treeMap[m_currentVar]->Branch("Jets_mv2c10", &Jets_mv2c10);
    m_treeMap[m_currentVar]->Branch("Jets_mv2c20", &Jets_mv2c20);

	///Z boson
    m_treeMap[m_currentVar]->Branch("Z_m", &Z_m);
    m_treeMap[m_currentVar]->Branch("Z_pT", &Z_pT);
    m_treeMap[m_currentVar]->Branch("Z_eta", &Z_eta);
    m_treeMap[m_currentVar]->Branch("Z_phi", &Z_phi);
    m_treeMap[m_currentVar]->Branch("Z_e", &Z_e);


    ///TruthStorage
	/*
    //m_treeMap[m_currentVar]->Branch("TruthStorage_MET_e", &TruthStorage_MET_e);
    //m_treeMap[m_currentVar]->Branch("TruthStorage_MET_phi", &TruthStorage_MET_phi);
    m_treeMap[m_currentVar]->Branch("TruthStorage_LLP_e", &TruthStorage_LLP_e);
    m_treeMap[m_currentVar]->Branch("TruthStorage_LLP_pT", &TruthStorage_LLP_pT);
    m_treeMap[m_currentVar]->Branch("TruthStorage_LLP_eta", &TruthStorage_LLP_eta);
    m_treeMap[m_currentVar]->Branch("TruthStorage_LLP_phi", &TruthStorage_LLP_phi);
    m_treeMap[m_currentVar]->Branch("TruthStorage_LLP_Lxy", &TruthStorage_LLP_Lxy);
    m_treeMap[m_currentVar]->Branch("TruthStorage_LLP_Lz", &TruthStorage_LLP_Lz);
    m_treeMap[m_currentVar]->Branch("TruthStorage_LLP_decayLength", &TruthStorage_LLP_decayLength);
    m_treeMap[m_currentVar]->Branch("TruthStorage_LLP_decayTime", &TruthStorage_LLP_decayTime);
    m_treeMap[m_currentVar]->Branch("TruthStorage_LLP_flighttime", &TruthStorage_LLP_flightTime);
    m_treeMap[m_currentVar]->Branch("TruthStorage_LLP_lifetime", &TruthStorage_LLP_lifetime);

    m_treeMap[m_currentVar]->Branch("TruthStorage_H_e", &TruthStorage_H_e);
    m_treeMap[m_currentVar]->Branch("TruthStorage_H_pT", &TruthStorage_H_pT);
    m_treeMap[m_currentVar]->Branch("TruthStorage_H_eta", &TruthStorage_H_eta);
    m_treeMap[m_currentVar]->Branch("TruthStorage_H_phi", &TruthStorage_H_phi);
    m_treeMap[m_currentVar]->Branch("TruthStorage_Z_e", &TruthStorage_Z_e);
    m_treeMap[m_currentVar]->Branch("TruthStorage_Z_pT", &TruthStorage_Z_pT);
    m_treeMap[m_currentVar]->Branch("TruthStorage_Z_eta", &TruthStorage_Z_eta);
    m_treeMap[m_currentVar]->Branch("TruthStorage_Z_phi", &TruthStorage_Z_phi);
    m_treeMap[m_currentVar]->Branch("TruthStorage_W_e", &TruthStorage_W_e);
    m_treeMap[m_currentVar]->Branch("TruthStorage_W_pT", &TruthStorage_W_pT);
    m_treeMap[m_currentVar]->Branch("TruthStorage_W_eta", &TruthStorage_W_eta);
    m_treeMap[m_currentVar]->Branch("TruthStorage_W_phi", &TruthStorage_W_phi);
	*/

}

void MVATree_Zemu::Reset(){
    EventWeight = 0;
    PileupWeight = 0;
    leptonSF_el = 0;
    leptonSF_mu = 0;
    JvtSF_jet = 0;
    UnPrescale = 0;
    LumiWeight = 0;
    MCEventWeight = 0;
    EventNumber = -1;
    mcChannelNumber = -1;
    mu = -1;
    mu_origin = -1;
    mu_weighted = -1;
    isBlinded = 0;

    trigSF = 0;

    ///Leptons
    Leptons_eventTriggers.clear();
    Leptons_nElectrons = 0;
    Leptons_electron_pT.clear();
    Leptons_electron_e.clear();
    Leptons_electron_eta.clear();
    Leptons_electron_phi.clear();
    Leptons_electron_pTcone20.clear();
    Leptons_electron_d0.clear();
    Leptons_electron_d0sig.clear();
    Leptons_electron_z0.clear();
    Leptons_electron_charge.clear();
    Leptons_electron_passCuts.clear();
    Leptons_electron_passLooserCuts.clear();
    Leptons_electron_isGradientIso.clear();
    Leptons_electron_passTriggers.clear();
    Leptons_electron_scaleFactor.clear();
    Leptons_electron_iso.clear();
    Leptons_electron_quality.clear();

	Leptons_electron_effSFReco.clear();
	Leptons_electron_effSFIsoGradientMediumLH.clear();
	Leptons_electron_effSFmediumLH.clear();
	Leptons_electron_trigSFmediumLHIsoGradient.clear();

    Leptons_nMuons = 0;
    Leptons_muon_pT.clear();
    Leptons_muon_e.clear();
    Leptons_muon_eta.clear();
    Leptons_muon_phi.clear();
    Leptons_muon_pTcone20.clear();
    Leptons_muon_d0.clear();
    Leptons_muon_d0sig.clear();
    Leptons_muon_z0.clear();
    Leptons_muon_charge.clear();
    Leptons_muon_passCuts.clear();
    Leptons_muon_passLooserCuts.clear();
    Leptons_muon_isGradientIso.clear();
    Leptons_muon_passTriggers.clear();
    Leptons_muon_scaleFactor.clear();
    Leptons_muon_iso.clear();
    Leptons_muon_quality.clear();

	Leptons_muon_TTVAEffSF.clear();
	Leptons_muon_effSF.clear();
	Leptons_muon_gradientIsoSF.clear();
	Leptons_muon_mediumEffSF.clear();

    ///MET
	/*
    MET_CST_met = -999;
    MET_CST_phi = -999;
    MET_TST_met = -999;
    MET_TST_phi = -999;
    CST_TST_Diff = -999;
    CST_TST_Diff_phi = -999;
	*/
    MET_met = -999;
    MET_phi = -999;


    ///Jets
    Jets_nJets = 0;
    Jets_nCRJets = 0;
    Jets_leading_pT = 0;
    Jets_subleading_pT = 0;
    Jets_pT.clear();
    Jets_E.clear();
    Jets_eta.clear();
    Jets_phi.clear();
    Jets_Jvt.clear();
    Jets_nTrks.clear();
    Jets_logRatio.clear();
    Jets_chFrac.clear();
    Jets_isClean.clear();
    Jets_isCR.clear();
    Jets_timing.clear();
    Jets_width.clear();

    Jets_JvtSF.clear();
    Jets_isBTagged.clear();
    Jets_nbjet = 0;
    Jets_btagSF = 0;
    Jets_mv2c10.clear();
    Jets_mv2c20.clear();

	///Z boson
    Z_m = -999;
    Z_pT = -999;
    Z_eta = -999;
    Z_phi = -999;
    Z_e = -999;
/*
	///Z boson
    Z_m.clear();
    Z_pT.clear();
    Z_eta.clear();
    Z_phi.clear();
    Z_e.clear();
*/


    ///Truth Information
	/*
    TruthStorage_LLP_e.clear();
    TruthStorage_LLP_pT.clear();
    TruthStorage_LLP_eta.clear();
    TruthStorage_LLP_phi.clear();
    TruthStorage_LLP_Lxy.clear();
    TruthStorage_LLP_Lz.clear();
    TruthStorage_LLP_decayLength.clear();
    TruthStorage_LLP_decayTime.clear();
    TruthStorage_LLP_flightTime.clear();
    TruthStorage_LLP_lifetime.clear();

    TruthStorage_H_e.clear();
    TruthStorage_H_pT.clear();
    TruthStorage_H_eta.clear();
    TruthStorage_H_phi.clear();

    TruthStorage_Z_e.clear();
    TruthStorage_Z_eta.clear();
    TruthStorage_Z_pT.clear();
    TruthStorage_Z_phi.clear();

    TruthStorage_W_e.clear();
    TruthStorage_W_pT.clear();
    TruthStorage_W_eta.clear();
    TruthStorage_W_phi.clear();
	*/
}

/*
void MVATree_Zemu::Fill() {

  if (!m_persistent) {
    return;
  }
  // TODO fill different trees for regions (MVA training)?
  //      -> could use HistNameSvc to decide

  // TODO impose limits on variables here, e.g.
  // if (mBB > 175e3) mBB = 175e3;
  // or also transformations like
  // mBB = mBB / (mBB + mean(mBB))

  TransformVars();

  if (m_currentVar == "") {
    Error("MVATree::Fill()", "Variation is not set! Did you call SetVariation in your fill function?");
    exit(EXIT_FAILURE);
  }

  if (m_treeMap.find(m_currentVar) == m_treeMap.end()){
    Error("MVATree::Fill()", "No tree registered for variation '%s'", m_currentVar.c_str());
    exit(EXIT_FAILURE);
  }

  printf("\ninside CxAODReader_Zemu/Root/MVATree_Zemu.cxx, MVATree_Zemu::Fill(),  m_currentVar: %s \n\n", m_currentVar.c_str());

  m_treeMap[m_currentVar]->Fill();
}
*/

void MVATree_Zemu::SetVariation(std::string variation){
  m_currentVar = variation;
}

void MVATree_Zemu::Fill() {
  //StaticSetBranches(m_analysisType, this);
  //printf("\ninside CxAODReader_Zemu/Root/MVATree_Zemu.cxx, MVATree_Zemu::Fill(),  m_currentVar: %s \n\n", m_currentVar.c_str());
  MVATree::Fill();
}

void MVATree_Zemu::ReadMVA() {
    return;
 }

void MVATree_Zemu::TransformVars() {
}














