#ifndef CxAODReader_AnalysisReader_Zemu_H
#define CxAODReader_AnalysisReader_Zemu_H

#include "CxAODReader/AnalysisReader.h"
#include "CxAODReader_Zemu/MVATree_Zemu.h"
#include "PileupReweighting/PileupReweightingTool.h"
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"
#include "CxAODTools_Zemu/EventSelection_Zemu.h"
#include <TH1F.h>


class AnalysisReader_Zemu : public AnalysisReader {
public:

    enum Type {Zemu};
    const xAOD::MissingET* m_met; //!

	//struct ZemuResult;

/*
    const xAOD::MissingET* m_met_cst; //!
    const xAOD::MissingET* m_met_tst; //!
    const xAOD::MissingETContainer* cst_cont; //!
    const xAOD::MissingETContainer* tst_cont; //!
    ObjectReader<xAOD::MissingETContainer> *m_METReader_cst; //!
    ObjectReader<xAOD::MissingETContainer> *m_METReader_tst; //!           // !
*/
protected:

    //virtual EL::StatusCode initializeReader() override;
    virtual EL::StatusCode initializeSelection() override;
    //virtual EL::StatusCode initializeSumOfWeights() override;
    virtual EL::StatusCode initializeTools() override;
    //EL::StatusCode initializeCorrsAndSysts() override;

	/*
    virtual EL::StatusCode setObjectsForOR(
          const xAOD::ElectronContainer*,
          const xAOD::PhotonContainer*,
          const xAOD::MuonContainer*,
          const xAOD::TauJetContainer*,
          const xAOD::JetContainer*,
          const xAOD::JetContainer*) override
    { return EL::StatusCode::SUCCESS; }
	*/

    EL::StatusCode fill_Zemu();

	bool passLepTrigger(double& triggerSF_nominal, ZemuResult selectionResult);
	bool truthCheck_TruthLevel(const xAOD::TruthParticleContainer *truthp);
	//int truthCheck_RecoLevel(const xAOD::TruthParticleContainer *truthp, TLorentzVector lep1, TLorentzVector lep2);

	bool m_use2DbTagCut;

	void compute_btagging ();

    //int m_analysisType; //!
    std::string m_analysisType; //!
    std::string m_signal_channel; //!

    //bool passesLooseCuts(const xAOD::Electron* electron);
    //bool passesLooseCuts(const xAOD::Muon* muon);
    //bool passesCuts(const xAOD::Electron* electron);
    //bool passesCuts(const xAOD::Muon* muon);
    //double getLogRatio(double EMF);
    //bool isCRjet(const xAOD::Jet* jet);

    //std::vector<const xAOD::Electron*>& el_pt25;
    //std::vector<const xAOD::Muon*>& mu_pt25;


    MVATree_Zemu* m_tree; //!

    //1Lep histo
    TH1D* m_hist_1lep_CutFlow[3]; //!
    TH1D* m_hist_1lep_CutFlow_noWght[3]; //!  unweighted version


	////////////////////////////// my own object cutflow histogram
	// electron cutflow
	TH1D* m_hist_el_CutFlow_noWght; //!  unweighted version
	TH1D* m_hist_el_CutFlow_withWght; //!  weighted version

	// muon cutflow
    TH1D* m_hist_mu_CutFlow_noWght; //!  unweighted version
    TH1D* m_hist_mu_CutFlow_withWght; //!  weighted version

    // jet cutflow
    TH1D* m_hist_jet_CutFlow_noWght; //!  unweighted version
    TH1D* m_hist_jet_CutFlow_withWght; //!  weighted version

	// my own 2lep event cutflow
	TH1D* m_hist_2lep_CutFlow_noWght; //!  unweighted version
	TH1D* m_hist_2lep_CutFlow; //!  weighted version

	TH1D* m_hist_averageInteractionsPerCrossing; //! averageInteractionsPerCrossing
	TH1D* m_hist_averageInteractionsPerCrossingRecalc; //! averageInteractionsPerCrossing

	TH1D* m_hist_Pileupweight; //! Pileupweight
	TH1D* m_hist_PileupweightRecalc; //! PileupweightRecalc

	// my own histograms for plotting
    // event info
	TH1D* m_hist_EventWeight; //!
	TH1D* m_hist_MCEventWeight; //!
	TH1D* m_hist_LumiWeight; //!
	TH1D* m_hist_PileupWeight; //!
	TH1D* m_hist_averageInteractionsPerCrossingRecalc_afterCuts; //! averageInteractionsPerCrossing after cuts

	TH1D* m_hist_leptonSF_el; //!
	TH1D* m_hist_leptonSF_mu; //!

	// electron kinematics(weighted)
	TH1D* m_hist_nElectrons; //!
	TH1D* m_hist_electron_pT; //!
	TH1D* m_hist_electron_e; //!
	TH1D* m_hist_electron_phi; //!
	TH1D* m_hist_electron_eta; //!

	// muon kinematics(weighted)
	TH1D* m_hist_nMuons; //!
	TH1D* m_hist_muon_pT; //!
	TH1D* m_hist_muon_e; //!
	TH1D* m_hist_muon_phi; //!
	TH1D* m_hist_muon_eta; //!

	// jet kinematics(weighted)
	TH1D* m_hist_nJets; //!
	TH1D* m_hist_jet_pT; //!
    TH1D* m_hist_leading_jet_pT; //!
	TH1D* m_hist_jet_e; //!
	TH1D* m_hist_jet_phi; //!
	TH1D* m_hist_jet_eta; //!

	// Met kinematics(weighted)
	TH1D* m_hist_MET_met; //!

	// Z(ee, mumu, emu) boson (weighted)
	TH1D* m_hist_Z_pT_weighted; //! 
	TH1D* m_hist_Z_eta_weighted; //! 
	TH1D* m_hist_Z_phi_weighted; //! 
	TH1D* m_hist_Z_e_weighted; //! 
	TH1D* m_hist_Z_mass_weighted; //!

    TH1D* m_hist_normalisation; //!

	TH2F *m_hist_cut_optimization; //!





    //typedef std::tuple<uint32_t, unsigned long long, float, string> RunEventMet; //!
    //std::set<RunEventMet> m_setRunEventMet; //!

    // trigger sf systematics
    //std::vector<std::string> m_triggerSystList;


public:
    AnalysisReader_Zemu();
    virtual EL::StatusCode histInitialize () override;
    virtual EL::StatusCode fileExecute () override;
    virtual EL::StatusCode finalize () override;
    //virtual EL::StatusCode execute () override;

    // this is needed to distribute the algorithm to the workers
    ClassDefOverride(AnalysisReader_Zemu, 1);
};

#endif















