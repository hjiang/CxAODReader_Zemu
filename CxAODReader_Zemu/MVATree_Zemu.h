#ifndef CxAODReader_MVATree_Zemu_H
#define CxAODReader_MVATree_Zemu_H

#include "CxAODReader/MVATree.h"
#include "EventLoop/Worker.h"
#include <vector>

class MVATree_Zemu : public MVATree {

protected:

  //int m_analysisType;
  std::string m_analysisType;

  virtual void AddBranch(TString name, Float_t* address, bool forReader);
  virtual void AddBranch(TString name, Int_t* address, bool forReader);
  virtual void AddBranch(TString name, Bool_t* address, bool forReader);

  virtual void SetBranches() override;
  virtual void TransformVars() override;

public:

//  MVATree_DB(bool persistent, bool readMVA, int analysisType, EL::Worker *wk);
//  MVATree_Zemu(bool persistent, bool readMVA, int analysisType, EL::Worker* wk, std::vector<std::string> variations, bool nominalOnly);
  MVATree_Zemu(bool persistent, bool readMVA, std::string analysisType, EL::Worker* wk, std::vector<std::string> variations, bool nominalOnly);

  ~MVATree_Zemu() {}

  void Reset() override;
  void SetVariation(std::string variation);
  void Fill();
  virtual void ReadMVA();


    ///Variables for output ttree
    float EventWeight;
    float MCEventWeight;
    float LumiWeight;
    float PileupWeight;
    float leptonSF_el;
    float leptonSF_mu;
    float JvtSF_jet;
    float UnPrescale;
    int EventNumber;
    int mcChannelNumber;
    float mu;
    float mu_origin;
    float mu_weighted;
    bool isBlinded;

	float trigSF;

    ///Lepton Information
    std::vector<bool> Leptons_eventTriggers;

    int Leptons_nElectrons;
    std::vector<double> Leptons_electron_pT;
    std::vector<double> Leptons_electron_e;
    std::vector<double> Leptons_electron_phi;
    std::vector<double> Leptons_electron_eta;
    std::vector<double> Leptons_electron_pTcone20;
    std::vector<double> Leptons_electron_d0;
    std::vector<double> Leptons_electron_d0sig;
    std::vector<double> Leptons_electron_z0;
    std::vector<short> Leptons_electron_charge;
    std::vector<bool> Leptons_electron_passCuts;
    std::vector<bool> Leptons_electron_passLooserCuts;
    std::vector<bool> Leptons_electron_isGradientIso;
    std::vector<std::vector<bool>> Leptons_electron_passTriggers;
    std::vector<double> Leptons_electron_scaleFactor;
    std::vector<double> Leptons_electron_iso;
    std::vector<int> Leptons_electron_quality;

    std::vector<double> Leptons_electron_effSFReco;
    std::vector<double> Leptons_electron_effSFIsoGradientMediumLH;
    std::vector<double> Leptons_electron_effSFmediumLH;
    std::vector<double> Leptons_electron_trigSFmediumLHIsoGradient;

    int Leptons_nMuons;
    std::vector<double> Leptons_muon_pT;
    std::vector<double> Leptons_muon_e;
    std::vector<double> Leptons_muon_phi;
    std::vector<double> Leptons_muon_eta;
    std::vector<double> Leptons_muon_pTcone20;
    std::vector<double> Leptons_muon_d0;
    std::vector<double> Leptons_muon_d0sig;
    std::vector<double> Leptons_muon_z0;
    std::vector<short> Leptons_muon_charge;
    std::vector<bool> Leptons_muon_passCuts;
    std::vector<bool> Leptons_muon_passLooserCuts;
    std::vector<bool> Leptons_muon_isGradientIso;
    std::vector<std::vector<bool>> Leptons_muon_passTriggers;
    std::vector<double> Leptons_muon_scaleFactor;
    std::vector<double> Leptons_muon_iso;
    std::vector<int> Leptons_muon_quality;

    std::vector<double> Leptons_muon_TTVAEffSF;
    std::vector<double> Leptons_muon_effSF;
    std::vector<double> Leptons_muon_gradientIsoSF;
    std::vector<double> Leptons_muon_mediumEffSF;

    ///MET
	/*
    double TruthStorage_MET_e;
    double TruthStorage_MET_phi;
    double MET_CST_met;
    double MET_CST_phi;
    double MET_TST_met;
    double MET_TST_phi;
    double CST_TST_Diff;
    double CST_TST_Diff_phi;
	*/
    double MET_met;
    double MET_phi;

    ///Jets
    int Jets_nJets;
    int Jets_nCRJets;
    double Jets_leading_pT;
    double Jets_subleading_pT;
    std::vector<double> Jets_pT;
    std::vector<double> Jets_E;
    std::vector<double> Jets_eta;
    std::vector<double> Jets_phi;
    std::vector<double> Jets_Jvt;
    std::vector<double> Jets_nTrks;
    std::vector<double> Jets_logRatio;
    std::vector<double> Jets_chFrac;
    std::vector<bool> Jets_isCR;
    std::vector<bool> Jets_isClean;
    std::vector<double> Jets_timing;
    std::vector<double> Jets_width;

	std::vector<double> Jets_JvtSF;
	std::vector<double> Jets_isBTagged;
	int Jets_nbjet;
	double Jets_btagSF;
    std::vector<double> Jets_mv2c10;
    std::vector<double> Jets_mv2c20;

	///Z boson
	double Z_m;
	double Z_pT;
	double Z_eta;
	double Z_phi;
	double Z_e;

    ///Truth Information
	/*
    std::vector<double> TruthStorage_LLP_e;
    std::vector<double> TruthStorage_LLP_pT;
    std::vector<double> TruthStorage_LLP_eta;
    std::vector<double> TruthStorage_LLP_phi;
    std::vector<double> TruthStorage_LLP_Lxy;
    std::vector<double> TruthStorage_LLP_Lz;
    std::vector<double> TruthStorage_LLP_decayLength;
    std::vector<double> TruthStorage_LLP_decayTime;
    std::vector<double> TruthStorage_LLP_flightTime;
    std::vector<double> TruthStorage_LLP_lifetime;
    //Higgs & Z
    std::vector<double> TruthStorage_H_e;
    std::vector<double> TruthStorage_H_pT;
    std::vector<double> TruthStorage_H_eta;
    std::vector<double> TruthStorage_H_phi;
    std::vector<double> TruthStorage_Z_e;
    std::vector<double> TruthStorage_Z_pT;
    std::vector<double> TruthStorage_Z_eta;
    std::vector<double> TruthStorage_Z_phi;
    std::vector<double> TruthStorage_W_e;
    std::vector<double> TruthStorage_W_pT;
    std::vector<double> TruthStorage_W_eta;
    std::vector<double> TruthStorage_W_phi;
    std::vector<double> TruthStorage_electron_e;
    std::vector<double> TruthStorage_electron_pT;
    std::vector<double> TruthStorage_electron_eta;
    std::vector<double> TruthStorage_electron_phi;
    std::vector<double> TruthStorage_muon_e;
    std::vector<double> TruthStorage_muon_pT;
    std::vector<double> TruthStorage_muon_eta;
    std::vector<double> TruthStorage_muon_phi;
	*/

};


#endif











